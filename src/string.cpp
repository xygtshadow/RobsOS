#include <stddef.h>
#include <stdint.h>
#include "memory.h"
#include "terminal.h"
#include "debug.h"

namespace string {

    /** \brief Gets the length of a null terminated string
     *
     * \param str: pointer to the string
     * \return length of string, not including null terminator
     *
     */

    size_t strlen(const char* str){
        size_t len = 0;
        // strings end with 0, false
        while(str[len])
            len++;
        return len;
    }

    /** \brief Compare two strings, returning -1 when left < right, 1 when left > right, and 0 when left == right
     *
     * \param left: pointer to the first string
     * \param right: pointer to the second string
     * \return int signifiying before/equal/after -1/0/1
     *
     */

    size_t strcmp(const char* left, const char* right){
        terminal::writestring("Comparing strings [");
        terminal::writestring(left);
        terminal::writestring("] and [");
        terminal::writestring(right);
        terminal::writeline("]");
        uint32_t ll = strlen(left);
        uint32_t rl = strlen(right);
        uint32_t stop;
        if(ll<rl)
            stop=ll;
        else stop=rl;
        for(size_t i=0;i<stop;i++){
            if(left[i]<right[i])
                return -1;
            if(left[i]>right[i])
                return 1;
        }
        if(ll<rl)
            return -1;
        if(rl<ll)
            return 1;
        return 0;
    }

    /** \brief Allocates memory and copies the contents from [start to start+len)
     *
     * \param str: pointer to the string to copy from
     * \param start: zero-based start index
     * \param len: amount of characters to copy, excluding null terminator
     * \return pointer to new copy of string
     *
     */

    char* strcpy(const char* str, size_t start, uint32_t len){
        terminal::writestring("Copying string [");
        terminal::writestring(str);
        terminal::writestring("] start: ");
        terminal::print_dword_dec(start);
        terminal::writestring(" len: ");
        terminal::print_dword_dec(len);
        terminal::endl();
        if(start+len > strlen(str)){
            terminal::writeline("Strcpy bound error");
            breakpoint();
        }
        uint32_t stop = len+1;
        char* newstr = (char*)memory::malloc(stop);
        for(uint32_t i=0;i<stop;i++){
            newstr[i] = str[start+i];
        }
        terminal::writestring("returning: ");
        terminal::writeline(newstr);
        return newstr;
    }

    /** \brief Finds the index of the first occurance of a string within a string
     *
     * \param str: string to searched
     * \param pat: string to search for
     * \return integer index of first occurance of pat in str. -1 if not found
     *
     */

    int32_t indexof(const char* str, const char* pat){
        size_t str_len = strlen(str);
        size_t pat_len = strlen(pat);
        uint32_t stop = str_len-pat_len;
        for(uint32_t i=0;i<stop;i++){
            bool match=true;
            for(uint32_t j=0;j<pat_len;j++){
                if(str[i+j]!=pat[j]){
                    match=false;
                    break;
                }
            }
            if(match)
                return i;
        }
        return -1;
    }
}
