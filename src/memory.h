#ifndef MEMORY_H
#define MEMORY_H

namespace memory{
    #define PAGES 1048576
    //map of free memory map, 1=free
    void identity_page(uint32_t page_index);
    void initialize();
    extern uint32_t free_map_physical[PAGES/32];
    extern uint32_t free_map_virtual[PAGES/32];
    void* allocate_physical_page();
    uint32_t allocate_virtual_page();
    void* malloc(uint32_t s);
    void* calloc(uint32_t s);
    void free(void* ptr);
    void zero(uint8_t *loc, size_t size);
    void print_details();
}

#endif // MEMORY_H
