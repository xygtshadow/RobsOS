#include <stddef.h>
#include <stdint.h>
#ifndef TERMINAL_H
#define TERMINAL_H

namespace terminal{
    /* Hardware text mode color constants. */
    enum colors {
        COLOR_BLACK,
        COLOR_BLUE,
        COLOR_GREEN,
        COLOR_CYAN,
        COLOR_RED,
        COLOR_MAGENTA,
        COLOR_BROWN,
        COLOR_LIGHT_GREY,
        COLOR_DARK_GREY,
        COLOR_LIGHT_BLUE,
        COLOR_LIGHT_GREEN,
        COLOR_LIGHT_CYAN,
        COLOR_LIGHT_RED,
        COLOR_LIGHT_MAGENTA,
        COLOR_LIGHT_BROWN,
        COLOR_WHITE,
    };

    uint8_t make_color(enum colors fg, enum colors bg);

    uint16_t make_vgaentry(char c, uint8_t color);

    const size_t WIDTH = 80;
    const size_t HEIGHT = 25;

    extern size_t row;
    extern size_t column;
    extern uint8_t color;
    extern uint16_t* buffer;

    void initialize();

    void setcolor(uint8_t color);

    void putentryat(char c, uint8_t color, size_t x, size_t y);

    void putchar(char c);

    void writestring(const char* data);
    void writeline(const char* data);

    char nibble_to_hexchar(uint8_t c);

    void print_byte_hex(uint8_t b);

    void print_word_hex(uint16_t w);

    void print_dword_hex(uint32_t d);

    void update_cursor(int row, int col);

    void print_dword_dec(uint32_t dw);

    void endl();
}
#endif
