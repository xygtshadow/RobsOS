//platform dependent floating-point constants
#include <float.h>

//macros for bitwise and logical operators
#include <iso646.h>

//platform dependent limits of fundemental integral types
#include <limits.h>

//macros for dealing with alignment of objects
#include <stdalign.h>

//allows functions to accept an indefinite number of arguments
#include <stdarg.h>

//allows bool in C
#include <stdbool.h>

//standard type definitions
#include <stddef.h>

//exact-width integer types
#include <stdint.h>

//includes noreturn macro, for functions that dont return
#include <stdnoreturn.h>
