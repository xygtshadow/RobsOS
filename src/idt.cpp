#include <stddef.h>
#include <stdint.h>
#include "idt.h"
#include "terminal.h"
#include "debug.h"

extern "C"{
    void interrupt_0();/**< interrupt handler function defined in boot.s */
    void interrupt_1();/**< interrupt handler function defined in boot.s */
    void interrupt_2();/**< interrupt handler function defined in boot.s */
    void interrupt_3();/**< interrupt handler function defined in boot.s */
    void interrupt_4();/**< interrupt handler function defined in boot.s */
    void interrupt_5();/**< interrupt handler function defined in boot.s */
    void interrupt_6();/**< interrupt handler function defined in boot.s */
    void interrupt_7();/**< interrupt handler function defined in boot.s */
    void interrupt_8();/**< interrupt handler function defined in boot.s */
    void interrupt_9();/**< interrupt handler function defined in boot.s */
    void interrupt_10();/**< interrupt handler function defined in boot.s */
    void interrupt_11();/**< interrupt handler function defined in boot.s */
    void interrupt_12();/**< interrupt handler function defined in boot.s */
    void interrupt_13();/**< interrupt handler function defined in boot.s */
    void interrupt_14();/**< interrupt handler function defined in boot.s */
    void interrupt_16();/**< interrupt handler function defined in boot.s */
    void interrupt_17();/**< interrupt handler function defined in boot.s */
    void interrupt_18();/**< interrupt handler function defined in boot.s */
    void interrupt_19();/**< interrupt handler function defined in boot.s */
    void interrupt_20();/**< interrupt handler function defined in boot.s */
    void interrupt_32();/**< interrupt handler function defined in boot.s */
    void interrupt_33();/**< interrupt handler function defined in boot.s */
    void interrupt_35();/**< interrupt handler function defined in boot.s */
    void interrupt_36();/**< interrupt handler function defined in boot.s */
    void interrupt_37();/**< interrupt handler function defined in boot.s */
    void interrupt_38();/**< interrupt handler function defined in boot.s */
    void interrupt_39();/**< interrupt handler function defined in boot.s */
    void interrupt_40();/**< interrupt handler function defined in boot.s */
    void interrupt_41();/**< interrupt handler function defined in boot.s */
    void interrupt_42();/**< interrupt handler function defined in boot.s */
    void interrupt_43();/**< interrupt handler function defined in boot.s */
    void interrupt_44();/**< interrupt handler function defined in boot.s */
    void interrupt_45();/**< interrupt handler function defined in boot.s */
    void interrupt_46();/**< interrupt handler function defined in boot.s */
    void interrupt_47();/**< interrupt handler function defined in boot.s */
    void interrupt_null();/**< interrupt handler function defined in boot.s */
}

namespace idt{

    /** \brief Creates an IDT descriptor, type determined by attr
     *
     * \param id: pointer to descriptor struct to modify
     * \param offset: pointer to handler
     * \param selector: gdt/ldt which contains handler
     * \param attr: attributes for type/bitness/present/dpl
     *
     */
    void create_descriptor(descriptor *id, uint32_t offset, uint16_t selector, uint8_t attr){
        id->offset_0_15 = offset & 0xFFFF;
        id->selector = selector;
        id->reserved = 0;
        id->attr_x_x_x_d_x_dpl_p = attr;//d:1=32bit, 0=16bit. p=present
        //dpl=privilege level, 0=kernel,3=user
        id->offset_16_31 = (offset>>16) & 0xFFFF;
    }

    //switch to new TSS
    /** \brief Creates a task gate descriptor, switches task on interrupt
     *
     * \param id: pointer to descriptor struct to modify
     * \param selector: gdt/ldt to switch to
     * \param dpl: descriptor privilege level 0=kernel, 3=user
     * \param p: present 1=true, 0=false
     *
     */
    void create_task_gate_descriptor(descriptor *id, uint16_t selector, uint8_t dpl, uint8_t p){
        create_descriptor(id, 0, selector, (5 | ((dpl&3)<<5) | ((p&1)<<7)));
    }

    //cli, handle, sti
    /** \brief Creates an interrupt gate descriptor, disables/reenables interrupts
     *
     * \param id: pointer to descriptor struct to modify
     * \param offset: poniter to handler
     * \param selector: gdt/ldt which contains handler
     * \param d: bitness, 1=32bit, 0=16bit
     * \param dpl: descriptor privilege level 0=kernel, 3=user
     * \param p: present 1=true, 0=false
     *
     */
    void create_interrupt_gate_descriptor(descriptor *id, uint32_t offset, uint16_t selector,
                                          uint8_t d, uint8_t dpl, uint8_t p){
        create_descriptor(id, offset, selector, (6|((d&1)<<3)) | ((dpl&3)<<5) | ((p&1)<<7));
    }

    //handle without disabling interrupts
        /** \brief Creates an trap gate descriptor, leaves interrupts alone
     *
     * \param id: pointer to descriptor struct to modify
     * \param offset: poniter to handler
     * \param selector: gdt/ldt which contains handler
     * \param d: bitness, 1=32bit, 0=16bit
     * \param dpl: descriptor privilege level 0=kernel, 3=user
     * \param p: present 1=true, 0=false
     *
     */
    void create_trap_gate_descriptor(descriptor *id, uint32_t offset, uint16_t selector,
                                     uint8_t d, uint8_t dpl, uint8_t p){
        create_descriptor(id,offset, selector, (7|((d&1)<<3)) | ((dpl&3)<<5) | ((p&1)<<7));
    }

    /** \brief Creates a selector, which is an 13 bit index with 3 bits of metadata
     *
     * \param rpl: requested privilege level
     * \param ti: type 0=gdt, 1=ldt
     * \return returns a uint16_t selector
     *
     */
    uint16_t create_selector(uint8_t rpl, uint8_t ti, uint16_t index){
        //ti: 0=gdt, 1=ldt
        return (rpl&3) | ((ti&1)<<2) | ((index&0x1FFF)<<3);
    }

    /** \brief Fills a descriptor table with 256 entries
     *
     * \param id: pointer to descriptor table
     *
     */
    void create_all(descriptor *id){
        create_interrupt_gate_descriptor(&id[0], (uint32_t)interrupt_0,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[1], (uint32_t)interrupt_1,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[2], (uint32_t)interrupt_2,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[3], (uint32_t)interrupt_3,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[4], (uint32_t)interrupt_4,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[5], (uint32_t)interrupt_5,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[6], (uint32_t)interrupt_6,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[7], (uint32_t)interrupt_7,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[8], (uint32_t)interrupt_8,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[9], (uint32_t)interrupt_9,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[10], (uint32_t)interrupt_10,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[11], (uint32_t)interrupt_11,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[12], (uint32_t)interrupt_12,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[13], (uint32_t)interrupt_13,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[14], (uint32_t)interrupt_14,
                                         create_selector(0,0,1), 1, 0, 1);
        create_descriptor(&id[15],0,0,0);//Intel reserved. Do not use.
        create_interrupt_gate_descriptor(&id[16], (uint32_t)interrupt_16,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[17], (uint32_t)interrupt_17,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[18], (uint32_t)interrupt_18,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[19], (uint32_t)interrupt_19,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[20], (uint32_t)interrupt_20,
                                         create_selector(0,0,1), 1, 0, 1);
        for(int i=21;i<32;i++)//Intel reserved. Do not use.
            create_descriptor(&id[i],0,0,0);

        create_interrupt_gate_descriptor(&id[32], (uint32_t)interrupt_32,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[33], (uint32_t)interrupt_33,
                                         create_selector(0,0,1), 1, 0, 1);
        create_descriptor(&id[34],0,0,0);//should never be raised
        create_interrupt_gate_descriptor(&id[35], (uint32_t)interrupt_35,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[36], (uint32_t)interrupt_36,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[37], (uint32_t)interrupt_37,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[38], (uint32_t)interrupt_38,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[39], (uint32_t)interrupt_39,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[40], (uint32_t)interrupt_40,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[41], (uint32_t)interrupt_41,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[42], (uint32_t)interrupt_42,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[43], (uint32_t)interrupt_43,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[44], (uint32_t)interrupt_44,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[45], (uint32_t)interrupt_45,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[46], (uint32_t)interrupt_46,
                                         create_selector(0,0,1), 1, 0, 1);
        create_interrupt_gate_descriptor(&id[47], (uint32_t)interrupt_47,
                                         create_selector(0,0,1), 1, 0, 1);
        for(int i=48;i<256;i++)
            create_interrupt_gate_descriptor(&id[i], (uint32_t)interrupt_null,
                                             create_selector(0,0,1), 1, 0, 1);
    }
}
