#include <stddef.h>
#include <stdint.h>

#include "terminal.h"
#include "string.h"
#include "debug.h"
#include "gdt.h"
#include "tss.h"
#include "idt.h"
#include "pic.h"
#include "keyboard.h"
#include "shell.h"
#include "multiboot.h"
#include "boot.h"
#include "memory.h"
#include "kernel.h"

gdt::segment_descriptor global_descriptor_table[4] __attribute__((aligned(8))) = {0};/**< GDT Struct, 4 entries: null, code, data, tss */
idt::descriptor interrupt_descriptor_table[256] __attribute__((aligned(8))) = {0};/**< IDT for all 256 interrupts */
tss tss_list[1] = {0};/**< One TSS for now */

/** \brief Entry point for the kernel
 *
 * \param mbt: pointer to the multiboot information struct created by grub
 * \param magic: Magic boot number. May not have been pushed to the stack?
 *
 */
extern "C" //use C linkage so the boot.s can call kernel_main
void kernel_main(multiboot_info* mbt, unsigned int magic){


    //initialize terminal interface
    terminal::initialize();

    multiboot::initialize(mbt, magic);

    //null descriptor
    gdt::create_segment_descriptor(&global_descriptor_table[0], 0, 0,
                                  0, 0, 0, 0,
                                  0, 0, 0, 0);
    //code segment
    gdt::create_segment_descriptor(&global_descriptor_table[1], 0xFFFFF, 0x0,
                                  14, 1, 0, 1,
                                  0, 0, 1, 1);
    //data segment
    gdt::create_segment_descriptor(&global_descriptor_table[2], 0xFFFFF, 0x0,
                                  2, 1, 0, 1,
                                  0, 0, 1, 1);

    gdt::create_segment_descriptor(&global_descriptor_table[3], sizeof(tss_list[0]), (uint32_t)&tss_list[0],
                                  9, 0, 0, 1,
                                  0, 0, 1, 0);

    setGdt(&global_descriptor_table[0], sizeof(global_descriptor_table));
    reloadSegments();

    idt::create_all(&interrupt_descriptor_table[0]);
    setIdt(&interrupt_descriptor_table[0], sizeof(interrupt_descriptor_table));

    pic::disable();
    pic::initialize(32, 40);
    pic::IRQ_clear_mask(1);//keyboard
    interrupts_enable();

    memory::initialize();

    terminal::color = terminal::make_color(terminal::COLOR_GREEN, terminal::COLOR_BLACK);
    terminal::writestring("\n                               Welcome ");
    terminal::color = terminal::make_color(terminal::COLOR_CYAN, terminal::COLOR_BLACK);
    terminal::writestring("to ");
    terminal::color = terminal::make_color(terminal::COLOR_RED, terminal::COLOR_BLACK);
    terminal::writestring("RobsOS.\n");
    terminal::color = terminal::make_color(terminal::COLOR_LIGHT_GREY, terminal::COLOR_BLACK);

    //keyboard::echo();

    shell::initialize();

    terminal::writestring(  "Kernel Start: ");
    terminal::print_dword_hex((uint32_t)&kernel_start);
    terminal::writestring("\nKernel End:   ");
    terminal::print_dword_hex((uint32_t)&kernel_end);
    terminal::putchar('\n');
    //terminal::writestring("\nkeyboard scan code set: ");
    //terminal::print_byte(keyboard::get_scan_code_set());
    //memory::debug_addr(&global_descriptor_table[0]);

    uint32_t* a = (uint32_t*)memory::malloc(8);//16
    *a = 0xabcd;
    log("a: ", *a);
    void* b = memory::malloc(4088);//4096
    void* c = memory::malloc(7000);//7008, 4096+2912
    void* d = memory::malloc(10000);//10008, 4096+4096+1816
    memory::free(a);
    memory::free(b);
    memory::free(c);
    memory::free(d);

    memory::print_details();
    for(;;)
        halt();

}
