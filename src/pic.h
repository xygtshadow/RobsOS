#ifndef PIC_H
#define PIC_H

#include <stddef.h>
#include <stdint.h>
namespace pic {
    void sendEOI(uint8_t irq);
    void initialize(uint16_t offset1, uint16_t offset2);
    void disable();
    void IRQ_set_mask(uint8_t IRQline);
    void IRQ_clear_mask(uint8_t IRQline);
    uint16_t get_irr(void);
    uint16_t get_isr(void);
    bool is_spurious(uint8_t IRQline);
}

#endif
