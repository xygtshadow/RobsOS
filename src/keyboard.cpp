#include <stddef.h>
#include <stdint.h>
#include "io.h"
#include "terminal.h"
#include "debug.h"
#include "keyboard.h"

#define PS2_DATA 0x60
#define PS2_STATUS 0x64
#define PS2_COMMAND 0x64

#define KB_ERR_1 0x00
#define KB_SELF_TEST_PASSED 0xAA
#define KB_ECHO 0xEE
#define KB_ACK 0xFA
#define KB_SELF_TEST_FAILED_1 0xFC
#define KB_SELF_TEST_FAILED_2 0xFD
#define KB_RESEND 0xFE
#define KB_ERR_2 0xFF
extern "C"{
    void interrupts_enable();
    void interrupts_disable();
}
namespace keyboard{
    kb_struct keyboard = {0};/**< Contains the status of all the keys and lights on the keyboard */

    struct callback_entry {
        function_pointer func[HOOK_LIMIT];//max 16 hooks per key for now
    };/**< A list of callbacks for a specific up/down hook for a specific key */

    struct callback_struct {
        callback_entry down[KEY_COUNT];
        callback_entry up[KEY_COUNT];
    }callback = {0};/**< A set of callback entries for a specific key */

    /** \brief Registers a function pointer as a callback to a key
     *
     * \param k: key to hook to
     * \param up: hook to release if true
     * \param func: function pointer to callback function
     * \return true if success, else false
     *
     */
    bool register_callback(key k, bool up, function_pointer func){
        bool success = false;
        if(up){
            for(uint32_t i=0;i<HOOK_LIMIT;++i)
                if(callback.up[k].func[i]==0){//test if null
                    callback.up[k].func[i]=func;
                    success=true;
                    break;
                }
        }
        else {
            for(uint32_t i=0;i<HOOK_LIMIT;++i)
                if(callback.down[k].func[i]==0){//test if null
                    callback.down[k].func[i]=func;
                    success=true;
                    break;
                }
        }
        return success;
    }

    /** \brief Gets status of num/caps/scroll lock keys in a useful format
     *
     * \return status byte
     *
     */
    uint8_t get_status_byte(){
        uint8_t b = 0x00;
        if(keyboard.scroll_lock_status)
            b=b|1;
        if(keyboard.num_lock_status)
            b=b|2;
        if(keyboard.caps_lock_status)
            b=b|4;
        return b;
    }

    /** \brief Updates the num/caps/scroll lock lights
     *
     */
    void update_status(){
        wait_for_write_buffer();
        outb(PS2_DATA, 0xED);
        wait_for_read_buffer();
        while(inb(PS2_DATA)==KB_RESEND) ;
        wait_for_write_buffer();
        outb(PS2_DATA, get_status_byte());
        wait_for_read_buffer();
        if(inb(PS2_DATA)==KB_RESEND)
            update_status();
    }

    /** \brief Calls all of the callback functions that are hooked to a specific key
     *
     * \param k: key to call callback functions on
     * \param up: call the up functions if true, down functions if false
     *
     */
    void do_callback(key k, bool up){
        //breakpoint();
        if(up){
            keyboard.keys[k]=false;
            for(uint32_t i=0;i<HOOK_LIMIT;++i)
                if(callback.up[k].func[i])
                    callback.up[k].func[i]();
        }
        else{
            keyboard.keys[k]=true;
            for(uint32_t i=0;i<HOOK_LIMIT;++i)
                if(callback.down[k].func[i])
                    callback.down[k].func[i]();
            if(k==key::caps_lock){
                keyboard.caps_lock_status = !keyboard.caps_lock_status;//todo update keyboard lights
                update_status();
            }
            if(k==key::num_lock){
                keyboard.num_lock_status = !keyboard.num_lock_status;
                update_status();
            }
            if(k==key::scroll_lock){
                keyboard.scroll_lock_status = !keyboard.scroll_lock_status;
                update_status();
            }
        }
    }

    /** \brief Checks if the PS2 buffer is ready for reading
     *
     * \return ready status
     *
     */
    bool read_buffer_ready(){
        return (inb(PS2_STATUS)&1)==1;
    }

    /** \brief Spinloop until read buffer is ready. This is probably a bad idea.
     *
     */
    void wait_for_read_buffer(){
        while(!read_buffer_ready())
            continue;
    }

    /** \brief Checks if the PS2 buffer is ready for writing
     *
     * \return ready status
     *
     */
    bool write_buffer_ready(){
        return (inb(PS2_STATUS)&2) == 0;
    }

    /** \brief Spinloop until write buffer is ready.
     *
     */
    void wait_for_write_buffer(){
        while(!write_buffer_ready())
            continue;
    }

    /** \brief This is called when they keyboard has an interrupt.
     *         Check the scan code then do_callback on that key
     *
     */
    void interrupt(){
        wait_for_read_buffer();//not necessary?
        switch(inb(PS2_DATA)){
            case 0x04: do_callback(key::three, false);break;
            case 0x08: do_callback(key::seven, false);break;
            case 0x0C: do_callback(key::hyphen, false);break;
            case 0x10: do_callback(key::q, false);break;
            case 0x14: do_callback(key::t, false);break;
            case 0x18: do_callback(key::o, false);break;
            case 0x1C: do_callback(key::enter, false);break;
            case 0x20: do_callback(key::d, false);break;
            case 0x24: do_callback(key::j, false);break;
            case 0x28: do_callback(key::quotation_mark, false);break;
            case 0x2C: do_callback(key::z, false);break;
            case 0x30: do_callback(key::b, false);break;
            case 0x34: do_callback(key::period, false);break;
            case 0x38: do_callback(key::left_alt, false);break;
            case 0x3C: do_callback(key::f2, false);break;
            case 0x40: do_callback(key::f6, false);break;
            case 0x44: do_callback(key::f10, false);break;
            case 0x48: do_callback(key::num_8, false);break;
            case 0x4C: do_callback(key::num_5, false);break;
            case 0x50: do_callback(key::num_2, false);break;
            case 0x58: do_callback(key::f12, false);break;
            case 0x84: do_callback(key::three, true);break;
            case 0x88: do_callback(key::seven, true);break;
            case 0x8C: do_callback(key::hyphen, true);break;
            case 0x90: do_callback(key::q, true);break;
            case 0x94: do_callback(key::t, true);break;
            case 0x98: do_callback(key::o, true);break;
            case 0x9C: do_callback(key::enter, true);break;
            case 0xA0: do_callback(key::d, true);break;
            case 0xA4: do_callback(key::j, true);break;
            case 0xA8: do_callback(key::quotation_mark, true);break;
            case 0xAC: do_callback(key::z, true);break;
            case 0xB0: do_callback(key::b, true);break;
            case 0xB4: do_callback(key::period, true); break;
            case 0xB8: do_callback(key::left_alt, true);break;
            case 0xBC: do_callback(key::f2, true);break;
            case 0xC0: do_callback(key::f6, true);break;
            case 0xC4: do_callback(key::f10, true);break;
            case 0xC8: do_callback(key::num_8, true);break;
            case 0xCC: do_callback(key::num_5, true);break;
            case 0xD0: do_callback(key::num_2, true);break;
            case 0xD8: do_callback(key::f12, true);break;
            case 0x01: do_callback(key::esc, false);break;
            case 0x05: do_callback(key::four, false);break;
            case 0x09: do_callback(key::eight, false);break;
            case 0x0D: do_callback(key::equals, false);break;
            case 0x11: do_callback(key::w, false);break;
            case 0x15: do_callback(key::y, false);break;
            case 0x19: do_callback(key::p, false);break;
            case 0x1D: do_callback(key::left_ctrl, false);break;
            case 0x21: do_callback(key::f, false);break;
            case 0x25: do_callback(key::k, false);break;
            case 0x29: do_callback(key::backtick, false);break;
            case 0x2D: do_callback(key::x, false);break;
            case 0x31: do_callback(key::n, false);break;
            case 0x35: do_callback(key::slash, false);break;
            case 0x39: do_callback(key::space, false);break;
            case 0x3D: do_callback(key::f3, false);break;
            case 0x41: do_callback(key::f7, false);break;
            case 0x45: do_callback(key::num_lock, false);break;
            case 0x49: do_callback(key::num_9, false);break;
            case 0x4D: do_callback(key::num_6, false);break;
            case 0x51: do_callback(key::num_3, false);break;
            case 0x81: do_callback(key::esc, true);break;
            case 0x85: do_callback(key::four, true);break;
            case 0x89: do_callback(key::eight, true);break;
            case 0x8D: do_callback(key::equals, true);break;
            case 0x91: do_callback(key::w, true);break;
            case 0x95: do_callback(key::y, true);break;
            case 0x99: do_callback(key::p, true);break;
            case 0x9D: do_callback(key::left_ctrl, true);break;
            case 0xA1: do_callback(key::f, true);break;
            case 0xA5: do_callback(key::k, true);break;
            case 0xA9: do_callback(key::backtick, true);break;
            case 0xAD: do_callback(key::x, true);break;
            case 0xB1: do_callback(key::n, true);break;
            case 0xB5: do_callback(key::slash, true);break;
            case 0xB9: do_callback(key::space, true);break;
            case 0xBD: do_callback(key::f3, true);break;
            case 0xC1: do_callback(key::f7, true);break;
            case 0xC5: do_callback(key::num_lock, true);break;
            case 0xC9: do_callback(key::num_9, true);break;
            case 0xCD: do_callback(key::num_6, true);break;
            case 0xD1: do_callback(key::num_3, true);break;
            case 0x02: do_callback(key::one, false);break;
            case 0x06: do_callback(key::five, false);break;
            case 0x0A: do_callback(key::nine, false);break;
            case 0x0E: do_callback(key::backspace, false);break;
            case 0x12: do_callback(key::e, false);break;
            case 0x16: do_callback(key::u, false);break;
            case 0x1A: do_callback(key::left_bracket, false);break;
            case 0x1E: do_callback(key::a, false);break;
            case 0x22: do_callback(key::g, false);break;
            case 0x26: do_callback(key::l, false);break;
            case 0x2A: do_callback(key::left_shift, false);break;
            case 0x2E: do_callback(key::c, false);break;
            case 0x32: do_callback(key::m, false);break;
            case 0x36: do_callback(key::right_shift, false);break;
            case 0x3A: do_callback(key::caps_lock, false);break;
            case 0x3E: do_callback(key::f4, false);break;
            case 0x42: do_callback(key::f8, false);break;
            case 0x46: do_callback(key::scroll_lock, false);break;
            case 0x4A: do_callback(key::num_hyphen, false);break;
            case 0x4E: do_callback(key::num_plus, false);break;
            case 0x52: do_callback(key::num_0, false);break;
            case 0x82: do_callback(key::one, true);break;
            case 0x86: do_callback(key::five, true);break;
            case 0x8A: do_callback(key::nine, true);break;
            case 0x8E: do_callback(key::backspace, true);break;
            case 0x92: do_callback(key::e, true);break;
            case 0x96: do_callback(key::u, true);break;
            case 0x9A: do_callback(key::left_bracket, true);break;
            case 0x9E: do_callback(key::a, true);break;
            case 0xA2: do_callback(key::g, true);break;
            case 0xA6: do_callback(key::l, true);break;
            case 0xAA: do_callback(key::left_shift, true);break;
            case 0xAE: do_callback(key::c, true); break;
            case 0xB2: do_callback(key::m, true);break;
            case 0xB6: do_callback(key::right_shift, true);break;
            case 0xBA: do_callback(key::caps_lock, true);break;
            case 0xBE: do_callback(key::f4, true);break;
            case 0xC2: do_callback(key::f8, true);break;
            case 0xC6: do_callback(key::scroll_lock, true);break;
            case 0xCA: do_callback(key::num_hyphen, true);break;
            case 0xCE: do_callback(key::num_plus, true);break;
            case 0xD2: do_callback(key::num_0, true);break;
            case 0x03: do_callback(key::two, false);break;
            case 0x07: do_callback(key::six, false);break;
            case 0x0B: do_callback(key::zero, false);break;
            case 0x0F: do_callback(key::tab, false);break;
            case 0x13: do_callback(key::r, false);break;
            case 0x17: do_callback(key::i, false);break;
            case 0x1B: do_callback(key::right_bracket, false);break;
            case 0x1F: do_callback(key::s, false);break;
            case 0x23: do_callback(key::h, false);break;
            case 0x27: do_callback(key::semi_colon, false);break;
            case 0x2B: do_callback(key::backslash, false);break;
            case 0x2F: do_callback(key::v, false);break;
            case 0x33: do_callback(key::comma, false);break;
            case 0x37: do_callback(key::num_asterisk, false);break;
            case 0x3B: do_callback(key::f1, false);break;
            case 0x3F: do_callback(key::f5, false);break;
            case 0x43: do_callback(key::f9, false);break;
            case 0x47: do_callback(key::num_7, false);break;
            case 0x4B: do_callback(key::num_4, false);break;
            case 0x4F: do_callback(key::num_1, false);break;
            case 0x53: do_callback(key::num_period, false);break;
            case 0x57: do_callback(key::f11, false);break;
            case 0x83: do_callback(key::two, true); break;
            case 0x87: do_callback(key::six, true);break;
            case 0x8B: do_callback(key::zero, true);break;
            case 0x8F: do_callback(key::tab, true);break;
            case 0x93: do_callback(key::r, true);break;
            case 0x97: do_callback(key::i, true);break;
            case 0x9B: do_callback(key::right_bracket, true);break;
            case 0x9F: do_callback(key::s, true);break;
            case 0xA3: do_callback(key::h, true);break;
            case 0xA7: do_callback(key::semi_colon, true);break;
            case 0xAB: do_callback(key::backslash, true);break;
            case 0xAF: do_callback(key::v, true);break;
            case 0xB3: do_callback(key::comma, true);break;
            case 0xB7: do_callback(key::num_asterisk, true);break;
            case 0xBB: do_callback(key::f1, true);break;
            case 0xBF: do_callback(key::f5, true);break;
            case 0xC3: do_callback(key::f9, true);break;
            case 0xC7: do_callback(key::num_7, true);break;
            case 0xCB: do_callback(key::num_4, true);break;
            case 0xCF: do_callback(key::num_1, true);break;
            case 0xD3: do_callback(key::num_period, true);break;
            case 0xD7: do_callback(key::f11, true);break;
            case 0xE0:{
                wait_for_read_buffer();
                switch(inb(PS2_DATA)){
                    case 0x1C: do_callback(key::num_enter, false);break;
                    case 0x38: do_callback(key::right_alt, false);break;
                    case 0x48: do_callback(key::up_arrow, false); break;
                    case 0x50: do_callback(key::down_arrow, false);break;
                    case 0x5C: do_callback(key::right_win, false);break;
                    case 0x9C: do_callback(key::num_enter, true);break;
                    case 0xB8: do_callback(key::right_alt, true);break;
                    case 0xC8: do_callback(key::up_arrow, true);break;
                    case 0xD0: do_callback(key::down_arrow, true);break;
                    case 0xDC: do_callback(key::right_win, true);break;
                    case 0x1D: do_callback(key::right_ctrl, false);break;
                    case 0x35: do_callback(key::num_slash, false);break;
                    case 0x49: do_callback(key::page_up, false);break;
                    case 0x4D: do_callback(key::right_arrow, false);break;
                    case 0x51: do_callback(key::page_down, false);break;
                    case 0x5D: do_callback(key::menu, false);break;
                    case 0x9D: do_callback(key::right_ctrl, true);break;
                    case 0xB5: do_callback(key::num_slash, true);break;
                    case 0xC9: do_callback(key::page_up, true);break;
                    case 0xCD: do_callback(key::right_arrow, true);break;
                    case 0xD1: do_callback(key::page_down, true);break;
                    case 0xDD: do_callback(key::menu, true);break;
                    case 0x52: do_callback(key::insert_key, false);break;
                    case 0xD2: do_callback(key::insert_key, true);break;
                    case 0x47: do_callback(key::home, false);break;
                    case 0x4B: do_callback(key::left_arrow, false);break;
                    case 0x4F: do_callback(key::end_key, false);break;
                    case 0x53: do_callback(key::delete_key, false);break;
                    case 0x5B: do_callback(key::left_win, false);break;
                    case 0xC7: do_callback(key::home, true);break;
                    case 0xCB: do_callback(key::left_arrow, true);break;
                    case 0xCF: do_callback(key::end_key, true);break;
                    case 0xD3: do_callback(key::delete_key, true);break;
                    case 0xDB: do_callback(key::left_win, true);break;
                    case 0x2A: {
                        wait_for_read_buffer();
                        if(inb(PS2_DATA)==0xE0){
                            wait_for_read_buffer();
                            if(inb(PS2_DATA)==0x37){
                                do_callback(key::print_screen, false);
                            } else {
                                terminal::writestring("0xE0, 0x2A, 0xE0, expected 0x37");
                            }
                        } else {
                            terminal::writestring("0xE0, 0x2A, expected 0xE0");
                        }
                        break;
                    }
                    case 0xB7: {
                        wait_for_read_buffer();
                        if(inb(PS2_DATA)==0xE0){
                            wait_for_read_buffer();
                            if(inb(PS2_DATA)==0xAA){
                                do_callback(key::print_screen, true);
                            } else {
                                terminal::writestring("0xE0, 0xB7, 0xE0, expected 0xAA");
                            }
                        } else {
                            terminal::writestring("0xE0, 0xB7, expected 0xE0");
                        }
                        break;
                    }
                    default: terminal::writestring("unknown key pressed");break;
                }
                break;
            }
            case 0xE1:{
                wait_for_read_buffer();
                if(inb(PS2_DATA)==0x1D){
                    wait_for_read_buffer();
                    if(inb(PS2_DATA)==0x45){
                        wait_for_read_buffer();
                        if(inb(PS2_DATA)==0xE1){
                            wait_for_read_buffer();
                            if(inb(PS2_DATA)==0x9D){
                                wait_for_read_buffer();
                                if(inb(PS2_DATA)==0xC5){
                                    do_callback(key::pause, false);
                                } else {
                                    terminal::writestring("0xE1, 0x1D, 0x45, 0xE1, 0x9D, expected 0xC5");
                                }
                            } else {
                                terminal::writestring("0xE1, 0x1D, 0x45, 0xE1, expected 0x9D");
                            }
                        } else {
                            terminal::writestring("0xE1, 0x1D, 0x45, expected 0xE1");
                        }
                    } else {
                        terminal::writestring("0xE1, 0x1D, expected 0x45");
                    }
                } else {
                    terminal::writestring("0xE1, expected 0x1D");
                }
                break;
            }
            case 0xEB:{
                wait_for_read_buffer();
                if(inb(PS2_DATA)==0xFF){
                    do_callback(key::left_win, false);
                } else {
                    terminal::writestring("0xEB, expected 0xFF");
                }
                break;
            }
            default: terminal::writestring("unknown key pressed or ack");break;
        }
    }

    /** \brief Sends an echo to the keyboard and waits for a response
     *
     */
    void echo(){
        uint8_t scan_code;
        terminal::writestring("sending echo ");
        wait_for_write_buffer();
        outb(PS2_DATA, 0xEE);
        wait_for_read_buffer();
        scan_code = inb(PS2_DATA);
        if(scan_code == 0xEE)
            terminal::writestring("echo received ");
        else terminal::print_byte_hex(scan_code);
    }

    //doesnt work
    /** \brief DOESNT WORK Gets the scan code set for the keyboard controller
     *
     * \return scancode set code
     *
     */
    uint8_t get_scan_code_set(){
        uint8_t scan_code;
        wait_for_write_buffer();
        outb(PS2_DATA,0xF0);//get/set scan code set command
        wait_for_read_buffer();
        scan_code = inb(PS2_DATA);
        while(scan_code == KB_RESEND){
            wait_for_write_buffer();
            outb(PS2_DATA, 0xF0);
            wait_for_read_buffer();
            scan_code = inb(PS2_DATA);
        }
        if(scan_code == KB_ACK){
            wait_for_write_buffer();//0 for get, 1/2/3 for set
            outb(PS2_DATA, 0);
            wait_for_read_buffer();
            scan_code = inb(PS2_DATA);
            while(scan_code == KB_RESEND){
                wait_for_write_buffer();
                outb(PS2_DATA, 0);
                wait_for_read_buffer();
                scan_code = inb(PS2_DATA);
            }
            if(scan_code == KB_ACK)
                scan_code = inb(PS2_DATA);
            return scan_code;//generates interrupt with code 0x02...
        } else {
            terminal::writestring("expected KB_ACK(FA), got: ");
            terminal::print_byte_hex(scan_code);
            return 0xFF;
        }
    }
}
