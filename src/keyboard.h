#ifndef KEYBOARD_H
#define KEYBOARD_H

namespace keyboard{
    const uint32_t KEY_COUNT = 104;
    const uint32_t HOOK_LIMIT = 16;

    struct kb_struct {
        bool keys[KEY_COUNT];
        bool num_lock_status; bool caps_lock_status; bool scroll_lock_status;
    };
    extern kb_struct keyboard;

    void interrupt();
    void echo();
    uint8_t get_scan_code_set();
    enum key{
        esc,
        f1,
        f2,
        f3,
        f4,
        f5,
        f6,
        f7,
        f8,
        f9,
        f10,
        f11,
        f12,
        backtick,
        one,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        zero,
        hyphen,
        equals,
        backspace,
        tab,
        q,
        w,
        e,
        r,
        t,
        y,
        u,
        i,
        o,
        p,
        left_bracket,
        right_bracket,
        backslash,
        caps_lock,
        a,
        s,
        d,
        f,
        g,
        h,
        j,
        k,
        l,
        semi_colon,
        quotation_mark,
        enter,
        left_shift,
        z,
        x,
        c,
        v,
        b,
        n,
        m,
        comma,
        period,
        slash,
        right_shift,
        left_ctrl,
        left_win,
        left_alt,
        space,
        right_alt,
        right_win,
        menu,
        right_ctrl,
        print_screen,
        scroll_lock,
        pause,
        insert_key,
        home,
        page_up,
        delete_key,
        end_key,
        page_down,
        up_arrow,
        left_arrow,
        down_arrow,
        right_arrow,
        num_lock,
        num_slash,
        num_asterisk,
        num_hyphen,
        num_7,
        num_8,
        num_9,
        num_4,
        num_5,
        num_6,
        num_plus,
        num_1,
        num_2,
        num_3,
        num_0,
        num_period,
        num_enter,
    };
    typedef void(*function_pointer)();
    bool register_callback(key k, bool up, function_pointer func);
    void wait_for_read_buffer();
    void wait_for_write_buffer();
}

#endif
