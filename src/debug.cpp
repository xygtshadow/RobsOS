#include <stddef.h>
#include <stdint.h>
#include "terminal.h"

/** \brief Triggers a Bochs breakpoint
 *
 */
void breakpoint(){
    asm("xchgw %bx, %bx");
}

/** \brief helper function for logging values to the terminal
 *
 * \param str: string to print before value
 * \param val: value to print in hex
 *
 */

void log(const char* str, uint32_t val){
    terminal::writestring(str);
    terminal::print_dword_hex(val);
    terminal::endl();
}
