#include <stddef.h>
#include <stdint.h>
#include "gdt.h"
#include "debug.h"

namespace gdt{

    /** \brief Creates a segment descriptor with all relevants bits in the right place
     *
     * \param sd: pointer to segment descriptor struct
     * \param segment_limit: size of segment in bytes if G=0, else 4KiB if G=1
     * \param base: address of byte 0 in linear address
     * \param type: if S=1 bytes = 0EWA (data, expand-down, write, accessed)
     *                  accessed bit is set when segment loaded into register
     *                  expand-down is for stack segments
     *              if S=1 bytes = 1CRA (code, conforming, read, accessed)
     *                  conforming means less privileged code can run
     *              if S=0, system descriptor. decimal values:
     *                   0,8,10,13 Reserved. 2 LDT, 5 Task Gate
     *              16bit: 1 TSS (Available), 3 TSS (Busy), 4 Call Gate, 6 Interrupt Gate, 7 Trap Gate
     *              32bit: 9 TSS (Available), 11 TSS (Busy), 12 Call Gate, 14 Interrupt Gate 16 Trap Gate
     * \param S: descriptor type, 1=code/data, 0=system
     * \param DPL: descriptor privilege level, 0=most privileged, 3=least privileged
     * \param P: segment-present, 0=segment not in memory, 1=segment in memory
     * \param D/B: default operation size/default stack pointer size/upper bound flag
                    if code, default length for addresses and operands 1=32 0=16/8
                    if SS, size of stack pointer 1=32, 0=16
                    if expand-down data segment, upper bound, 1=4GiB, 0=64KiB
     * \param G: granularity flag determined segment limit units, 0=bytes, 1=4KiB
     * \param L: 64-bit code segment flag, ia-32e mode data segment with 64bit code
     * \param AVL: available, available for use by system software
     * \return
     *
     *
     */
    void create_segment_descriptor(segment_descriptor *sd,
                                       uint32_t segment_limit, uint32_t base,
                                       uint8_t type, uint8_t s, uint8_t dpl, uint8_t p,
                                       uint8_t avl, uint8_t l, uint8_t db, uint8_t g){
        sd->segment_limit_0_15 = segment_limit & 0x0000FFFF;
        sd->base_0_15 = base & 0x0000FFFF;
        sd->base_16_23 = (base & 0x00FF0000)>>16;
        sd->type_s_dpl_p = (type&0xF)|((s&1)<<4)|((dpl&3)<<5)|((p&1)<<7);
        sd->segment_limit_16_19_avl_l_db_g = ((segment_limit&0x000F0000)>>16)|((avl&1)<<4)|
                                             ((l&1)<<5)|((db&1)<<6)|((g&1)<<7);
        sd->base_24_31 = (base & 0xFF000000)>>24;
    }
}
