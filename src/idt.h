#ifndef IDT_H
#define IDT_H

namespace idt{
    struct descriptor{
        uint16_t offset_0_15;//empty for task gate
        uint16_t selector;
        uint8_t reserved;//always 0
        //           1 1 1 1 1 2   1
        uint8_t attr_x_x_x_d_x_dpl_p;
        uint16_t offset_16_31;//empty for task gate
    }__attribute__((packed));
    
    void create_task_gate_descriptor(descriptor *id, uint16_t selector, uint8_t dpl, uint8_t p);

    void create_interrupt_gate_descriptor(descriptor *id, uint32_t offset, uint16_t selector, 
                                          uint8_t d, uint8_t dpl, uint8_t p);
    
    void create_trap_gate_descriptor(descriptor *id, uint32_t offset, uint16_t selector, 
                                     uint8_t d, uint8_t dpl, uint8_t p);
    
    void create_all(descriptor *id);
}

#endif
