#include <stddef.h>
#include <stdint.h>
#include "multiboot.h"
#include "terminal.h"
#include "debug.h"
#include "boot.h"
#include "memory.h"
#include "kernel.h"

namespace multiboot {
    multiboot_info* mbt_info;/**< pointer to the mbt info struct, for safekeeping */
    unsigned int magic;/**< the magic number for multiboot, UNTESTED */

    /** \brief Tests if an address is divisible by 4096
     *
     * \param addr: The address to be tested
     * \return True if addr is divisible by 4096
     *
     */
    bool is_4k_aligned(uint32_t addr){
        return addr%4096==0;
    }

    /** \brief Tests if given address is 4k aligned, if it is then it returns given address, else returns next 4k aligned address
     *
     * \param addr: address to test for 4k
     * \return returns given address or next 4k aligned address
     *
     */

    uint32_t get_next_4k_addr(uint32_t addr){
        if(!is_4k_aligned(addr)){
            return (addr/4096+1)*4096;
        } else return addr;
    }

    /** \brief Initializes the memory map
     *
     * \param mbt: pointer to multiboot_info from grub
     * \param magic: multiboot magic number UNUSED MAYBE BROKEN
     *
     */

    void initialize(multiboot_info* mbt, unsigned int magic){
        // magic should == 0x2BADB002
        mbt_info = mbt;
        uint32_t free_bytes = 0;
        memory_map_t* mmap = (memory_map_t*)(mbt->mmap_addr);//load mmap struct for a section of contiguous memory
        //mbt has mmap table information, loop through each
        while(mmap < (memory_map_t*)(mbt->mmap_addr + mbt->mmap_length)) {
            //size: size of struct
            //type: 1=available
            //base_addr_low: low 32bits of beginning of memory area
            //base_addr_high: high 32bits of beginning of memory area, 0 in protected mode?
            //length_low: low 32bits of size of memory area
            //length_high: high 32bits of size of memory area, 0 in protected mode?
            if((mmap->type==1)&&(mmap->base_addr_low>=1048576)){//dont use bottom 1MB
                uint32_t base=get_next_4k_addr(mmap->base_addr_low);
                terminal::writestring("base: ");
                terminal::print_dword_hex(base);
                uint32_t length=mmap->length_low;
                terminal::writestring(" length: ");
                terminal::print_dword_hex(length);
                uint32_t end_addr=base+length;
                terminal::writestring(" end: ");
                terminal::print_dword_hex(end_addr);
                terminal::putchar('\n');
                //1bit=4096kb
                for(uint32_t i=base;i<end_addr;i+=4096){
                    if((i+4096<base+length)&&(
                                       (i+4096<(uint32_t)&kernel_start)||
                                       (i>((uint32_t)&kernel_end))
                                       )){//if full page is free
                        uint32_t page_pos = i/4096;
                        uint32_t free_map_index=page_pos/32;
                        uint8_t map_shift = page_pos%32;
                        memory::free_map_physical[free_map_index] = memory::free_map_physical[free_map_index] | (1<<map_shift);
                        memory::free_map_virtual[free_map_index] = memory::free_map_virtual[free_map_index] | (1<<map_shift);
                        free_bytes+=4096;
                    }
                }
            }
            mmap = (memory_map_t*) ( (unsigned int)mmap + mmap->size + sizeof(unsigned int) );
        }/*
        for(size_t i=0;i<32768;i++){
            if(memory::free_map[i]>0){
                terminal::writestring("index: ");
                terminal::print_dword(i);
                terminal::writestring(" value: ");
                terminal::print_dword(free_map[i]);
                terminal::putchar('\n');
            }
        }*/
        terminal::writestring("Free bytes: ");
        terminal::print_dword_dec(free_bytes);
        terminal::putchar('\n');
        terminal::writestring("Magic number: ");
        terminal::print_dword_hex(magic);
    }
}
