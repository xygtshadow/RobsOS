#ifndef MATH_H
#define MATH_H
#include <stddef.h>
#include <stdint.h>

namespace math{
    uint32_t pow(uint32_t base, uint32_t exp);
}

#endif // MATH_H
