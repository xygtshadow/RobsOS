#ifndef GDT_H
#define GDT_H

#include <stddef.h>
#include <stdint.h>

namespace gdt{
    struct segment_descriptor {
        uint16_t segment_limit_0_15;
        uint16_t base_0_15;
        uint8_t base_16_23;
        //      4    1 2   1
        uint8_t type_s_dpl_p;
        //      4                   1   1 1  1
        uint8_t segment_limit_16_19_avl_l_db_g;
        uint8_t base_24_31;
    }__attribute__((packed)); 

    void create_segment_descriptor(segment_descriptor *sd, uint32_t segment_limit, uint32_t base,
                                uint8_t type, uint8_t s, uint8_t dpl, uint8_t p, uint8_t avl,
                                uint8_t l, uint8_t db, uint8_t g);
}
    
#endif
