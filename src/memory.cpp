#include <stddef.h>
#include <stdint.h>
#include "multiboot.h"
#include "memory.h"
#include "terminal.h"
#include "debug.h"
#include "kernel.h"
#include "boot.h"

#define MEMORY_DEBUG 1

namespace memory{

    #define PAGE_SIZE 4096
    #define BLOCK_HEADER_SIZE 8

    struct used_page{// 520 bytes
        uint32_t page_index;
        uint32_t block_size;
        uint32_t page_map[128];//each bit represents a byte of the page
    }__attribute__((packed));

    struct used_pages_ll{//a page has enough room for 7 used_page structs and a pointer to the next used_pages_ll struct 3672 bytes
        used_page used_page_list[7];
        used_pages_ll *next_used_page_ll;
    }__attribute__((packed));

    struct page_table_struct{
        uint32_t page_table[1024];
    }__attribute__((packed));

    uint32_t free_map_physical[PAGES/32] = {0};/**< A bitmap of free pages, mapindex, used for paging */
    uint32_t free_map_virtual[PAGES/32] = {0};/**< A bitmap of free pages, mapindex, used for malloc */

    used_pages_ll used_pages = {0};

    bool paging_enabled = false;

    /** \brief Converts a pageindex into a memory address,
     *         Multiplies pageindex by page size in bytes
     *
     * \param i: pageindex
     * \return page address
     *
     */
    void* convert_page_to_addr(uint32_t i){
        return (void*)(i*PAGE_SIZE);//page size = 4kb
    }

    /** \brief Converts an mapindex-offset pair into a pageindex
     *
     * \param index: mapindex
     * \param offset: bit in the mapindex's uint32_t
     * \return pageindex
     *
     */
    uint32_t convert_index_offset_to_page(size_t index, uint8_t offset){
        return index*32+offset;
    }

    void* get_physaddr(void* virtualaddr){
        log("get_psyaddr: ", (uint32_t)virtualaddr);
        uint32_t pdi = (uint32_t)virtualaddr>>22;
        uint32_t pti = (uint32_t)virtualaddr>>12&0x03FF;
        uint32_t* pd = (uint32_t*)0xFFFFF000;
        if((pd[pdi]&1)==0){
            terminal::writeline("pde not present");
            breakpoint();
        }
        uint32_t* pt = ((uint32_t*)0xFFC00000)+(0x400*pdi);
        if((pt[pti]&1)==0){
            terminal::writeline("pti not present");
            breakpoint();
        }
        return (void*)((pt[pti] & ~0xFFF) + ((uint32_t)virtualaddr & 0xFFF));
    }

    void map_page(void* physaddr, void* virtualaddr, uint32_t present, uint32_t write,
                  uint32_t user, uint32_t writethrough, uint32_t disablecache, uint32_t global){
        if(!paging_enabled){
            terminal::writeline("Error: attempting to map_page before paging has been enabled.");
            breakpoint();
        }
        log("Mapping page (virtualaddr): ", (uint32_t)virtualaddr);
        log(" to page (physaddr): ", (uint32_t)physaddr);
        uint32_t pdi = (uint32_t)virtualaddr>>22;
        uint32_t pti = (uint32_t)virtualaddr>>12&0x03FF;
        uint32_t* pd = (uint32_t*)0xFFFFF000;
        if((pd[pdi]&1)==0){
            log("Error: page table not present: ", pd[pdi]);
            breakpoint();
        }
        uint32_t* pt = ((uint32_t*)0xFFC00000)+(0x400*pdi);
        uint32_t pt_addr = ((uint32_t)pt)&0xFFFFF000;
        uint32_t pt_present = ((uint32_t)pt)&1;
        if(pt_addr==0 || pt_present==0){
            log(" no page table exists (pde): ",(uint32_t)pt);
            void* newpage = allocate_physical_page();
            *pt = (((uint32_t)newpage)&0xFFFFF000)|11;//present, write, writethrough
        }
        //uint32_t pte_addr = (uint32_t)pt[pti]&0xFFFFF000;
        uint32_t pte_present = (uint32_t)pt[pti]&1;
        if(pte_present==1){
            log("Error (already mapped) mapping page: ", (uint32_t)pt);
            breakpoint();
        }

        uint32_t flags = ((present&1)<<0)|
                         ((write&1)<<1)|
                         ((user&1)<<2)|
                         ((writethrough&1)<<3)|
                         ((disablecache&1)<<4)|
                         ((global&1)<<7);
        pt[pti]=((uint32_t)physaddr) | (flags & 0xFFF);
        update_tlb();
    }

    void map_page_prepaging(uint32_t* page_directory, void* physaddr, void* virtualaddr, uint32_t present, uint32_t write,
                  uint32_t user, uint32_t writethrough, uint32_t disablecache, uint32_t global){
        log("prepaging Mapping page (virtualaddr): ", (uint32_t)virtualaddr);
        log(" to page (physaddr): ", (uint32_t)physaddr);
        uint32_t pdi = (uint32_t)virtualaddr>>22;
        uint32_t pti = (uint32_t)virtualaddr>>12&0x03FF;
        uint32_t pde = page_directory[pdi];
        uint32_t pt_addr = ((uint32_t)pde)&0xFFFFF000;
        uint32_t pt_present = ((uint32_t)pde)&1;
        if(pt_addr==0 || pt_present==0){
            log(" no page table exists (pde): ", pde);
            void* newpage = allocate_physical_page();
            page_directory[pdi] = (((uint32_t)newpage)&0xFFFFF000)|11;//present, write, writethrough: 8+2+1
            pt_addr = ((uint32_t)newpage)&0xFFFFF000;
        }
        uint32_t* pt = (uint32_t*)pt_addr;
        //uint32_t pte_addr = (uint32_t)(pt[pti]&0xFFFFF000);
        uint32_t pte_present = (uint32_t)(pt[pti]&1);
        if(pte_present==1){
            log("Error (already mapped) mapping page: ", (uint32_t)pt);
            breakpoint();
        }

        uint32_t flags = ((present&1)<<0)|
                         ((write&1)<<1)|
                         ((user&1)<<2)|
                         ((writethrough&1)<<3)|
                         ((disablecache&1)<<4)|
                         ((global&1)<<7);
        pt[pti]=((uint32_t)physaddr) | (flags & 0xFFF);
    }

    /** \brief Initialize the paging system
     *
     */

    void initialize(){
        uint32_t* page_directory = (uint32_t*)allocate_physical_page();
        zero((uint8_t*) page_directory, PAGE_SIZE);
        page_directory[1023] = (((uint32_t)page_directory)&0xFFFFF000)|0xB;//last entry points to page directory

        uint32_t* pt = (uint32_t*)allocate_physical_page();
        zero((uint8_t*) pt, PAGE_SIZE);

        page_directory[0] = (((uint32_t)pt)&0xFFFFF000)|0xB;
        ///////////////////
        //identity map bottom 1MiB==256 pages
        for(uint32_t page_index=0;page_index<256;page_index++){
            map_page_prepaging(page_directory, (void*)(page_index*PAGE_SIZE), (void*)(page_index*PAGE_SIZE), 1, 1, 0, 1, 0, 0);
        }
        //kernel start to kernel end
        uint32_t kstart = ((uint32_t)(&kernel_start))/PAGE_SIZE;
        uint32_t kend = ((uint32_t)(&kernel_end))/PAGE_SIZE;
        if(((uint32_t)(&kernel_end))%PAGE_SIZE!=0)
            kend++;
        log("kernel start: ", kstart);
        log("kend: ", kend);
        for(uint32_t i=kstart; i<kend; i++){
            map_page_prepaging(page_directory, (void*)(i*PAGE_SIZE), (void*)(i*PAGE_SIZE), 1, 1, 0, 1, 0, 0);
        }

        terminal::writeline("Enabling paging.");
        breakpoint();
        enable_paging((uint32_t)page_directory);
        paging_enabled=true;
        terminal::writeline("Paging enabled.");
    }

    /** \brief Searches for a free physical page, sets it as not free, returns pageindex,
     *          terribly inefficient and naive
     *          dereferencing this pointer after paging has been enable is a bad idea
     * \return pageindex
     *
     */
    void* allocate_physical_page(){
        uint32_t limit = PAGES/32;
        for(uint32_t i=0;i<limit;++i){
            if(free_map_physical[i]>0){
                for(uint8_t j=0;j<32;j++){
                    if((free_map_physical[i]&(1<<j))>0){
                        uint32_t page_index = convert_index_offset_to_page(i,j);
                        #ifdef MEMORY_DEBUG
                        log("allocating page for paging: ", page_index);
                        #endif // MEMORY_DEBUG
                        free_map_physical[i]=free_map_physical[i]&(~(1<<j));
                        return convert_page_to_addr(page_index);
                    }
                }
            }
        }
        terminal::writeline("Error: unable to allocate a physical page.");
        breakpoint();
        return 0;
    }

    /** \brief Searches for a free page, sets it as not free, returns pageindex,
     *          terribly inefficient and naive
     * \return pageindex
     *
     */
    uint32_t allocate_virtual_page(){
        if(!paging_enabled){
            terminal::writeline("Malloc before paging initialized.");
            breakpoint();
        }
        uint32_t limit = PAGES/32;
        for(uint32_t i=0;i<limit;++i){
            if(free_map_virtual[i]>0){
                for(uint8_t j=0;j<32;j++){
                    if((free_map_virtual[i]&(1<<j))>0){
                        uint32_t page_index = convert_index_offset_to_page(i,j);
                        #ifdef MEMORY_DEBUG
                        terminal::writestring("allocating page for malloc: ");
                        terminal::print_dword_hex(page_index);
                        terminal::endl();
                        #endif // MEMORY_DEBUG
                        free_map_virtual[i]=free_map_virtual[i]&(~(1<<j));
                        map_page(allocate_physical_page(), (void*)(page_index*PAGE_SIZE), 1, 1, 0, 1, 0, 0);
                        return page_index;
                    }
                }
            }
        }
        terminal::writeline("Error: unable to allocate a page.");
        breakpoint();
        return 0;
    }

    /** \brief Allocates multiple contiguous pages for malloc
     *
     * \param num: number of contiguous pages
     * \return first free page index
     *
     */

    uint32_t allocate_virtual_pages(size_t num){
        if(!paging_enabled) return 0;
        uint32_t limit = PAGES/32;
        for(uint32_t i=0;i<limit;++i){
            if(free_map_virtual[i]>0){
                for(uint8_t j=0;j<32;j++){
                    if((free_map_virtual[i]&(1<<j))>0){
                        bool free = true;
                        for(uint32_t k=1;k<=num;k++){
                            uint32_t modified_index = i + ((j + k) / 32);
                            uint32_t modified_shift = (j + k) % 32;
                            if((free_map_virtual[modified_index]&(1<<modified_shift))==0){
                                free = false;
                                j+=k;
                                break;
                            }
                        }
                        if(free){
                            for(uint32_t k=0;k<=num;k++){
                                uint32_t modified_index = i + ((j + k) / 32);
                                uint32_t modified_shift = (j + k) % 32;
                                free_map_virtual[modified_index] = free_map_virtual[modified_index]&(~(1<<modified_shift));//wizardry
                                uint32_t page_index = convert_index_offset_to_page(modified_index,modified_shift);
                                map_page(allocate_physical_page(), convert_page_to_addr(page_index), 1, 1, 0, 1, 0, 1);
                            }
                            #if MEMORY_DEBUG
                            terminal::writestring("allocating ");
                            terminal::print_dword_dec(num);
                            terminal::writestring(" pages starting at : ");
                            terminal::print_dword_hex(convert_index_offset_to_page(i,j));
                            terminal::endl();
                            #endif // MEMORY_DEBUG
                            return convert_index_offset_to_page(i,j);
                        }
                    }
                }
            }
        }
        breakpoint();
        return 0;
    }

    /** \brief Marks a portion of a page as used
     *
     * \param page: page index to mark
     * \param start: byte offset to start marking
     * \param size: size in bytes to mark
     *
     */

    void mark_page(uint32_t page, uint32_t start, uint32_t size, bool used, uint32_t block_size){
        #if MEMORY_DEBUG
        terminal::writestring("marking page: ");
        terminal::print_dword_hex(page);
        terminal::endl();
        terminal::writestring(" start: ");
        terminal::print_dword_dec(start);
        terminal::endl();
        terminal::writestring(" size: ");
        terminal::print_dword_dec(size);
        terminal::endl();
        #endif // MEMORY_DEBUG
        used_pages_ll* upll = &used_pages;
        while(upll != 0){
            for(uint32_t i=0;i<7;i++){
                if(upll->used_page_list[i].page_index == page){
                    for(uint32_t j=start;j<start+size;j++){
                        uint32_t idx=j/32;
                        uint32_t off=j%32;
                        if(used){
                            upll->used_page_list[i].page_map[idx] = upll->used_page_list[i].page_map[idx] | (1<<off);
                        } else {
                            upll->used_page_list[i].page_map[idx] = upll->used_page_list[i].page_map[idx] & (~(1<<off));
                        }
                    }
                    return;
                }
            }
            upll=upll->next_used_page_ll;
        }
        //page not in used page list
        if(used){
            upll = &used_pages;
            while(upll != 0){
                for(uint32_t i=0;i<7;i++){
                    if(upll->used_page_list[i].page_index==0){
                        upll->used_page_list[i].page_index=page;
                        upll->used_page_list[i].block_size=block_size;
                        for(uint32_t j=start;j<start+size;j++){
                            uint32_t idx=j/32;
                            uint32_t off=j%32;
                            upll->used_page_list[i].page_map[idx] = upll->used_page_list[i].page_map[idx] | (1<<off);
                        }
                        return;
                    }
                }
                if(upll->next_used_page_ll==0){
                    upll->next_used_page_ll=(used_pages_ll*)allocate_virtual_page();
                    zero((uint8_t*)convert_page_to_addr((uint32_t)upll->next_used_page_ll),PAGE_SIZE);
                }
                upll=upll->next_used_page_ll;
            }
            terminal::writeline("unable to mark page as used.");
            breakpoint();
            return;
        }
        terminal::writeline("unable to free data, page not found.");
        breakpoint();
    }

    /** \brief Creates an 8 byte header for all dynamic memory
     *
     * The first 4 bytes are the size of the block, including the 8 byte header
     * The next 4 bytes are the bitwise negation of the first 4, to detect corruption
     *
     * \param addr: (integer) address of the beginning of the memory block, including header
     * \param size: size of the memory block, including header
     * \return address of the data, not including header
     *
     */

    void* create_block_header(uint32_t addr, uint32_t size){
        uint32_t* s = (uint32_t*)addr;
        *s = size;
        uint32_t* c = (uint32_t*)(addr+4);
        *c = ~size;
        return (void*)(addr+BLOCK_HEADER_SIZE);
    }

    uint32_t get_block_size(uint32_t s){
        //minimum 16, maximum PAGE_SIZE
        for(uint32_t i=16;i<=PAGE_SIZE;i*=2){
            if(i>=s) return i;
        }
        log("Error, unable to get block size for: ", s);
        breakpoint();
        return 4096;//this is unacceptable
    }

    /** \brief Allocates free memory, returns a pointer to the beginning
     *
     * \param s: size in bytes to allocate
     * \return void pointer to beginning of memory allocated
     *
     */
    void* malloc(uint32_t s){
        uint32_t bytes_needed = s+BLOCK_HEADER_SIZE;
        if(bytes_needed<PAGE_SIZE){
            uint32_t block_size = get_block_size(bytes_needed);
            #if MEMORY_DEBUG
            log("Allocating bytes: ", s);
            #endif // MEMORY_DEBUG
            used_pages_ll* upll = &used_pages;
            while(upll != 0){
                for(uint32_t i=0;i<7;i++){
                    if(upll->used_page_list[i].page_index != 0 && (upll->used_page_list[i].block_size == block_size)){
                        uint32_t contiguous_bytes = 0;
                        uint32_t page_index = upll->used_page_list[i].page_index;
                        uint32_t start_offset = 0;
                        for(uint32_t j=0;j<128;j++){
                            for(uint32_t k=0;k<32;k+=16){
                                if((upll->used_page_list[i].page_map[j] & (1<<k))==0){
                                    if(contiguous_bytes==0){
                                        start_offset = j*32+k;
                                    }
                                    contiguous_bytes++;
                                    if(contiguous_bytes==bytes_needed){
                                        //mark and return
                                        mark_page(page_index, start_offset, bytes_needed, true, block_size);
                                        uint32_t addr = ((uint32_t)convert_page_to_addr(page_index))+start_offset;//store address as integer
                                        #if MEMORY_DEBUG
                                        terminal::writestring("allocated ");
                                        terminal::print_dword_dec(bytes_needed);
                                        terminal::writestring(" bytes at ");
                                        terminal::print_dword_hex(addr);
                                        terminal::endl();
                                        #endif // MEMORY_DEBUG
                                        return create_block_header(addr, bytes_needed);
                                    }
                                } else contiguous_bytes = 0;
                            }
                        }
                    }
                }
                upll=upll->next_used_page_ll;
            }
            //not enough free space in used pages
            #if MEMORY_SMALL_MALLOC_DEBUG
            terminal::writeline("Not enough space found in currently allocated pages.");
            #endif // MEMORY_SMALL_MALLOC_DEBUG
            uint32_t newpage = allocate_virtual_page();
            mark_page(newpage, 0, bytes_needed, true, block_size);
            uint32_t addr = (uint32_t)convert_page_to_addr(newpage);
            return create_block_header(addr, bytes_needed);
        } else if (bytes_needed==PAGE_SIZE){
            terminal::writeline("Allocating exacly 1 page.");
            uint32_t page_index = allocate_virtual_page();
            uint32_t page_address = (uint32_t)convert_page_to_addr(page_index);
            mark_page(page_index, 0, bytes_needed, true, PAGE_SIZE);
            terminal::writeline("Page marked.");
            return create_block_header(page_address, bytes_needed);
        } else if (bytes_needed%PAGE_SIZE==0){
            size_t page_count = s/PAGE_SIZE;
            uint32_t first_page_index = allocate_virtual_pages(page_count);
            for(uint32_t i=0;i<page_count;i++){
                mark_page(first_page_index + i,0,PAGE_SIZE, true, PAGE_SIZE);
            }
            uint32_t addr = (uint32_t)convert_page_to_addr(first_page_index);
            return create_block_header(addr, bytes_needed);
        } else {
            size_t full_page_count = bytes_needed/PAGE_SIZE;//# of full pages to mark
            uint32_t first_page_index = allocate_virtual_pages(full_page_count+1);
            for(uint32_t i=0;i<full_page_count;i++){
                mark_page(first_page_index+i, 0, PAGE_SIZE, true, PAGE_SIZE);
            }
            mark_page(first_page_index+full_page_count,0,bytes_needed%PAGE_SIZE, true, get_block_size(bytes_needed%PAGE_SIZE));//mark remaining bytes in the last page
            uint32_t addr = (uint32_t)convert_page_to_addr(first_page_index);
            return create_block_header(addr, bytes_needed);
        }
    }

    /** \brief Allocates memory that is zeroed
     *
     * malloc's the space then zeroes it
     *
     * \param s: bytes to allocate
     * \return pointer to the space
     *
     */

    void* calloc(uint32_t s){
        void* ptr = malloc(s);
        zero((uint8_t*)ptr, s);
        return ptr;
    }

    /** \brief frees a pointer allocated by malloc, calloc, or realloc (eventually)
     *
     * \param ptr: the pointer which was returned by malloc, calloc, or realloc
     *
     */

    void free(void* ptr){
        terminal::writeline("freeing block");
        uint32_t size = *(((uint32_t*)ptr)-2);
        uint32_t checksum = *(((uint32_t*)ptr)-1);
        if((size^checksum) == 0xFFFFFFFF){
            terminal::writeline("valid checksum.");
        } else {
            terminal::writeline("invalid checksum.");
            log("size: ", size);
            log("checksum: ", checksum);
            breakpoint();
        }

        uint32_t page_index = ((uint32_t)ptr)/PAGE_SIZE;
        log("page index: ", page_index);
        uint32_t page_count = size/PAGE_SIZE;
        if(size%PAGE_SIZE!=0) page_count++;
        log("page count: ", page_count);

        uint32_t start_offset=(((uint32_t)ptr)-8)%PAGE_SIZE;
        log("start offset: ", start_offset);
        uint32_t end_offset=(start_offset+size)%PAGE_SIZE;
        if(end_offset==0) end_offset = PAGE_SIZE;//cant end at the start of a page, because size
        log("end offset: ", end_offset);

        for(uint32_t p=0;p<page_count;p++){
            if(p==0){//first page
                uint32_t s = PAGE_SIZE-start_offset;
                if(page_count==1)
                    s = end_offset-start_offset;
                mark_page(page_index+p,start_offset,s,false, get_block_size(s));
            } else if(p<(page_count-1)){//middle pages
                mark_page(page_index+p,0,PAGE_SIZE,false, PAGE_SIZE);
            } else {//last page
                mark_page(page_index+p,0,end_offset,false, get_block_size(end_offset));
            }
        }
    }

    /** \brief Zeroes the memory at loc to loc+size
     *
     * \param loc: location of memory to zero
     * \param size: size of memory to zero in bytes
     *
     */

    void zero(uint8_t *loc, size_t size){
        for(size_t i = 0;i<size;i++){
            loc[i]=0;
        }
    }

    /** \brief goes through and prints data in the used page list
     *
     */

    void print_details(){
        terminal::writeline("Printing memory details");
        used_pages_ll* upll = &used_pages;
        while(upll != 0){
            for(int i=0;i<7;i++){
                uint32_t page_index = upll->used_page_list[i].page_index;
                uint32_t block_size = upll->used_page_list[i].block_size;
                if(upll->used_page_list[i].page_index != 0){
                    log("Page Index: ", page_index);
                    log("Block Size: ", block_size);
                }
            }
            upll = upll->next_used_page_ll;
        }
    }
}
