#include <stddef.h>
#include <stdint.h>
#include "terminal.h"
#include "string.h"
#include "debug.h"
#include "keyboard.h"
#include "shell.h"
#include "memory.h"

namespace shell {

    char input[80] = {0};
    uint8_t pos = 0;

    void do_command(){
        size_t input_length = string::strlen((char*)&input);
        char* inputstr = string::strcpy((char*)&input, 0, input_length);
        int32_t space_index = string::indexof(inputstr, " ");
        char* commandstr;
        char* remstr;
        if(space_index<1){
            commandstr = string::strcpy(inputstr, 0, space_index-1);
            remstr = string::strcpy(inputstr, space_index+1, string::strlen(inputstr)-string::strlen(commandstr)-1);
        } else {
            commandstr = string::strcpy(inputstr, 0, input_length);
            remstr = string::strcpy("",0,0);
        }
        if(string::strcmp(commandstr, "echo")==0){
            terminal::writeline(remstr);
        }
        else {
            terminal::writeline("command not recognized.");
        }
        memory::free(inputstr);
        memory::free(commandstr);
        memory::free(remstr);

        for(size_t i=0;i<80;i++)
            terminal::putentryat(' ', terminal::color, i, 24);
        terminal::update_cursor(24,0);
        memory::zero((uint8_t*)&input, sizeof(input));
        pos=0;
    }

    /** \brief Inserts a character into ther terminal and updates the cursor
     *
     * \param c: character to insert
     *
     */

    void insert_char(char c){
        if(pos<80){
            terminal::putentryat(c, terminal::color, pos, 24);
            input[pos] = c;
            ++pos;
            terminal::update_cursor(24,pos);
        }
    }

    /** \brief Checks if a character should be capitalized
     *
     * \return boolean value capital
     *
     */

    bool capital(){
        //if shift or caps but not both or neither
        if((keyboard::keyboard.keys[keyboard::key::left_shift] || keyboard::keyboard.keys[keyboard::key::left_shift]) != (keyboard::keyboard.caps_lock_status)){
            return true;
        } else return false;
    }
    /** \brief Stubs to handle keyboard callbacks
     *
     */

    void a(){if(capital()) insert_char('A'); else insert_char('a');}
    void b(){if(capital()) insert_char('B'); else insert_char('b');}
    void c(){if(capital()) insert_char('C'); else insert_char('c');}
    void d(){if(capital()) insert_char('D'); else insert_char('d');}
    void e(){if(capital()) insert_char('E'); else insert_char('e');}
    void f(){if(capital()) insert_char('F'); else insert_char('f');}
    void g(){if(capital()) insert_char('G'); else insert_char('g');}
    void h(){if(capital()) insert_char('H'); else insert_char('h');}
    void i(){if(capital()) insert_char('I'); else insert_char('i');}
    void j(){if(capital()) insert_char('J'); else insert_char('j');}
    void k(){if(capital()) insert_char('K'); else insert_char('k');}
    void l(){if(capital()) insert_char('L'); else insert_char('l');}
    void m(){if(capital()) insert_char('M'); else insert_char('m');}
    void n(){if(capital()) insert_char('N'); else insert_char('n');}
    void o(){if(capital()) insert_char('O'); else insert_char('o');}
    void p(){if(capital()) insert_char('P'); else insert_char('p');}
    void q(){if(capital()) insert_char('Q'); else insert_char('q');}
    void r(){if(capital()) insert_char('R'); else insert_char('r');}
    void s(){if(capital()) insert_char('S'); else insert_char('s');}
    void t(){if(capital()) insert_char('T'); else insert_char('t');}
    void u(){if(capital()) insert_char('U'); else insert_char('u');}
    void v(){if(capital()) insert_char('V'); else insert_char('v');}
    void w(){if(capital()) insert_char('W'); else insert_char('w');}
    void x(){if(capital()) insert_char('X'); else insert_char('x');}
    void y(){if(capital()) insert_char('Y'); else insert_char('y');}
    void z(){if(capital()) insert_char('Z'); else insert_char('z');}
    void enter(){do_command();}
    void space(){insert_char(' ');}

    /** \brief Initialize/register keyboard callbacks for the shell, using the above stubs
     *
     */

    void initialize(){
        keyboard::register_callback(keyboard::key::a, false, a);
        keyboard::register_callback(keyboard::key::b, false, b);
        keyboard::register_callback(keyboard::key::c, false, c);
        keyboard::register_callback(keyboard::key::d, false, d);
        keyboard::register_callback(keyboard::key::e, false, e);
        keyboard::register_callback(keyboard::key::f, false, f);
        keyboard::register_callback(keyboard::key::g, false, g);
        keyboard::register_callback(keyboard::key::h, false, h);
        keyboard::register_callback(keyboard::key::i, false, i);
        keyboard::register_callback(keyboard::key::j, false, j);
        keyboard::register_callback(keyboard::key::k, false, k);
        keyboard::register_callback(keyboard::key::l, false, l);
        keyboard::register_callback(keyboard::key::m, false, m);
        keyboard::register_callback(keyboard::key::n, false, n);
        keyboard::register_callback(keyboard::key::o, false, o);
        keyboard::register_callback(keyboard::key::p, false, p);
        keyboard::register_callback(keyboard::key::q, false, q);
        keyboard::register_callback(keyboard::key::r, false, r);
        keyboard::register_callback(keyboard::key::s, false, s);
        keyboard::register_callback(keyboard::key::t, false, t);
        keyboard::register_callback(keyboard::key::u, false, u);
        keyboard::register_callback(keyboard::key::v, false, v);
        keyboard::register_callback(keyboard::key::w, false, w);
        keyboard::register_callback(keyboard::key::x, false, x);
        keyboard::register_callback(keyboard::key::y, false, y);
        keyboard::register_callback(keyboard::key::z, false, z);
        keyboard::register_callback(keyboard::key::enter, false, enter);
        keyboard::register_callback(keyboard::key::space, false, space);
    }
}
