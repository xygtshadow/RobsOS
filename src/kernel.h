#ifndef KERNEL_H
#define KERNEL_H

extern "C" {
    extern uint32_t kernel_start;//in boot.s
    extern uint32_t kernel_end;//in end.s
}

#endif // KERNEL_H

