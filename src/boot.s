# -*-gas-mode-*-
# Declare constants for multiboot header
.set ALIGN,    1<<0            # align loaded modules on page boundaries
.set MEMINFO,  1<<1            # provide memory map
.set FLAGS,    ALIGN | MEMINFO # this is the multiboot 'flag' field
.set MAGIC,    0x1BADB002      # 'magic number' lets bootloader find the header
.set CHECKSUM, -(MAGIC+FLAGS)  # checksum of above, to prove we are multiboot

# Declare a multiboot header that marks the program as a kernel. These are magic
# values that are documented in the multiboot standard. The bootloader will
# search for this signature in the first 8 KiB of the kernel file, aligned at a
# 32-bit boundary. The signature is in its own section so the header can be
# forced to be within the first 8 KiB of the kernel file.
.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

# The multiboot standard does not define the value of the stack pointer register
# (esp) and it is up to the kernel to provide a stack. This allocates room for a
# small stack by creating a symbol at the bottom of it, then allocating 16384
# bytes for it, and finally creating a symbol at the top. The stack grows
# downwards on x86. The stack is in its own section so it can be marked nobits,
# which means the kernel file is smaller because it does not contain an
# uninitialized stack. The stack on x86 must be 16-byte aligned according to the
# System V ABI standard and de-facto extensions. The compiler will assume the
# stack is properly aligned and failure to align the stack will result in
# undefined behavior.
kernel_start:
.section .bss
.align 16
stack_bottom:
.skip 16384 # 16KiB
stack_top:

# The linker script specifies _start as the entry point to the kernel and the
# bootloader will jump to this position once the kernel has been loaded. It
# doesn't make sense to return from this function as the bootloader is gone.

.section .text
.global _start
.type _start, @function
_start:
	# The bootloader has loaded us into 32-bit protected mode on a x86
	# machine. Interrupts are disabled. Paging is disabled. The processor
	# state is as defined in the multiboot standard. The kernel has full
	# control of the CPU. The kernel can only make use of hardware features
	# and any code it provides as part of itself. There's no printf
	# function, unless the kernel provides its own <stdio.h> header and a
	# printf implementation. There are no security restrictions, no
	# safeguards, no debugging mechanisms, only what the kernel provides
	# itself. It has absolute and complete power over the
	# machine.

	# To set up a stack, we set the esp register to point to the top of our
	# stack (as it grows downwards on x86 systems). This is necessarily done
	# in assembly as languages such as C cannot function without a stack.
	mov $stack_top, %esp
	mov $stack_top, %ebp
	# This is a good place to initialize crucial processor state before the
	# high-level kernel is entered. It's best to minimize the early
	# environment where crucial features are offline. Note that the
	# processor is not fully initialized yet: Features such as floating
	# point instructions and instruction set extensions are not initialized
	# yet. The GDT should be loaded here. Paging should be enabled here.
	# C++ features such as global constructors and exceptions will require
	# runtime support to work as well.

	#floating point instructions

	#instruction set extensions

	#paging

	#idt

	# Enter the high-level kernel. The ABI requires the stack is 16-byte
	# aligned at the time of the call instruction (which afterwards pushes
	# the return pointer of size 4 bytes). The stack was originally 16-byte
	# aligned above and we've since pushed a multiple of 16 bytes to the
	# stack since (pushed 0 bytes so far) and the alignment is thus
	# preserved and the call is well defined.

	push %eax #multiboot magic number
	push %ebx #pointer to multiboot_info structure
	call kernel_main

	# If the system has nothing more to do, put the computer into an
	# infinite loop. To do that:
	# 1) Disable interrupts with cli (clear interrupt enable in eflags).
	#    They are already disabled by the bootloader, so this is not needed.
	#    Mind that you might later enable interrupts and return from
	#    kernel_main (which is sort of nonsensical to do).
	# 2) Wait for the next interrupt to arrive with hlt (halt instruction).
	#    Since they are disabled, this will lock up the computer.
	# 3) Jump to the hlt instruction if it ever wakes up due to a
	#    non-maskable interrupt occurring or due to system management mode.
	cli
1:	hlt
	jmp 1b

#void halt();
.global halt
.type halt, @function
halt:
    hlt
    ret

#gdt
gdtr: .word 0
      .int 0
.global setGdt
.type setGdt, @function
setGdt:
    movl 4(%esp), %eax
    movl %eax, 2+gdtr
    movw 8(%esp), %ax
    movw %ax, gdtr
    lgdt gdtr
    ret

#ldt
ldtr: .word 0
      .int 0
.global reloadSegments
.type reloadSegments, @function
reloadSegments:
    # reload CS register containing code selector:
    ljmpl $0x08, $.reload_CS # 0x08 is gdt entry #1, ljmpl loads first operand into CS
.reload_CS:
    # reload data segment registers
    movw $0x10, %ax # 0x10 is gdt entry #2
    movw %ax, %ds # data segment
    movw %ax, %es # extra segment
    movw %ax, %fs # extra segment
    movw %ax, %gs # extra segment
    movw %ax, %ss # extra segment
    lldt ldtr #invalidates the ldtr
    ret

#in/out
.global inb
.type inb, @function
#byte inb(port)
inb:
    movw 4(%esp), %dx#port
    inb %dx, %al#read byte from %dx to %al
    ret

.global outb
.type outb, @function
#void outb(port, byte)
outb:
    movw 4(%esp), %dx#port
    movb 8(%esp), %al#byte
    outb %al, %dx#write byte %al to %dx
    ret

.global io_wait
.type io_wait, @function
#void io_wait()
io_wait:
    movb $0, %al
    outb %al, $0x80
    ret

#interrupts
.global interrupts_enable
.type interrupts_enable, @function
interrupts_enable:
    sti
    ret

.global interrupts_disable
.type interrupts_disable, @function
interrupts_disable:
    cli
    ret

idtr: .word 0 # idt limit
      .int 0 # idt base address
.global setIdt
.type setIdt, @function
setIdt:
    movl 4(%esp), %eax
    movl %eax, 2+idtr
    movw 8(%esp), %ax
    movw %ax, idtr
    lidt idtr
    #sti
    #int $0
    ret

.global enable_paging
.type enable_paging, @function
enable_paging:
    pushl %eax
    movl 8(%esp), %eax
    movl %eax, %cr3
    movl %cr0, %eax
    orl $0x80000000, %eax
    movl %eax, %cr0
    popl %eax
    ret

.global update_tlb
.type update_tlb, @function
update_tlb:
    pushl %eax
    movl %cr3,%eax
	movl %eax,%cr3
	popl %eax
	ret

.global interrupt_null
.align 4
.type interrupt_null, @function
interrupt_null:
    iretl

.global interrupt_0
.align 4
.type interrupt_0, @function
interrupt_0:
    pushal
    call interrupt_handler_0
    popal
    iretl

.global interrupt_1
.align 4
.type interrupt_1, @function
interrupt_1:
    pushal
    call interrupt_handler_1
    popal
    iretl

.global interrupt_2
.align 4
.type interrupt_2, @function
interrupt_2:
    pushal
    call interrupt_handler_2
    popal
    iretl

.global interrupt_3
.align 4
.type interrupt_3, @function
interrupt_3:
    pushal
    call interrupt_handler_3
    popal
    iretl

.global interrupt_4
.align 4
.type interrupt_4, @function
interrupt_4:
    pushal
    call interrupt_handler_4
    popal
    iretl

.global interrupt_5
.align 4
.type interrupt_5, @function
interrupt_5:
    pushal
    call interrupt_handler_5
    popal
    iretl

.global interrupt_6
.align 4
.type interrupt_6, @function
interrupt_6:
    pushal
    call interrupt_handler_6
    popal
    iretl

.global interrupt_7
.align 4
.type interrupt_7, @function
interrupt_7:
    pushal
    call interrupt_handler_7
    popal
    iretl

.global interrupt_8
.align 4
.type interrupt_8, @function
interrupt_8:
    pushal
    call interrupt_handler_8
    popal
    iretl

.global interrupt_9
.align 4
.type interrupt_9, @function
interrupt_9:
    pushal
    call interrupt_handler_9
    popal
    iretl

.global interrupt_10
.align 4
.type interrupt_10, @function
interrupt_10:
    pushal
    call interrupt_handler_10
    popal
    iretl

.global interrupt_11
.align 4
.type interrupt_11, @function
interrupt_11:
    pushal
    call interrupt_handler_11
    popal
    iretl

.global interrupt_12
.align 4
.type interrupt_12, @function
interrupt_12:
    pushal
    call interrupt_handler_12
    popal
    iretl

.global interrupt_13
.align 4
.type interrupt_13, @function
interrupt_13:
    pushal
    call interrupt_handler_13
    popal
    iretl

.global interrupt_14
.align 4
.type interrupt_14, @function
interrupt_14:
    pushal
    call interrupt_handler_14
    popal
    iretl

.global interrupt_16
.align 4
.type interrupt_16, @function
interrupt_16:
    pushal
    call interrupt_handler_16
    popal
    iretl

.global interrupt_17
.align 4
.type interrupt_17, @function
interrupt_17:
    pushal
    call interrupt_handler_17
    popal
    iretl

.global interrupt_18
.align 4
.type interrupt_18, @function
interrupt_18:
    pushal
    call interrupt_handler_18
    popal
    iretl

.global interrupt_19
.align 4
.type interrupt_19, @function
interrupt_19:
    pushal
    call interrupt_handler_19
    popal
    iretl

.global interrupt_20
.align 4
.type interrupt_20, @function
interrupt_20:
    pushal
    call interrupt_handler_20
    popal
    iretl

.global interrupt_32
.align 4
.type interrupt_32, @function
interrupt_32:
    pushal
    call interrupt_handler_32
    popal
    iretl

.global interrupt_33
.align 4
.type interrupt_33, @function
interrupt_33:
    pushal
    call interrupt_handler_33
    popal
    iretl

.global interrupt_35
.align 4
.type interrupt_35, @function
interrupt_35:
    pushal
    call interrupt_handler_35
    popal
    iretl

.global interrupt_36
.align 4
.type interrupt_36, @function
interrupt_36:
    pushal
    call interrupt_handler_36
    popal
    iretl

.global interrupt_37
.align 4
.type interrupt_37, @function
interrupt_37:
    pushal
    call interrupt_handler_37
    popal
    iretl

.global interrupt_38
.align 4
.type interrupt_38, @function
interrupt_38:
    pushal
    call interrupt_handler_38
    popal
    iretl

.global interrupt_39
.align 4
.type interrupt_39, @function
interrupt_39:
    pushal
    call interrupt_handler_39
    popal
    iretl

.global interrupt_40
.align 4
.type interrupt_40, @function
interrupt_40:
    pushal
    call interrupt_handler_40
    popal
    iretl

.global interrupt_41
.align 4
.type interrupt_41, @function
interrupt_41:
    pushal
    call interrupt_handler_41
    popal
    iretl

.global interrupt_42
.align 4
.type interrupt_42, @function
interrupt_42:
    pushal
    call interrupt_handler_42
    popal
    iretl

.global interrupt_43
.align 4
.type interrupt_43, @function
interrupt_43:
    pushal
    call interrupt_handler_43
    popal
    iretl

.global interrupt_44
.align 4
.type interrupt_44, @function
interrupt_44:
    pushal
    call interrupt_handler_44
    popal
    iretl

.global interrupt_45
.align 4
.type interrupt_45, @function
interrupt_45:
    pushal
    call interrupt_handler_45
    popal
    iretl

.global interrupt_46
.align 4
.type interrupt_46, @function
interrupt_46:
    pushal
    call interrupt_handler_46
    popal
    iretl

.global interrupt_47
.align 4
.type interrupt_47, @function
interrupt_47:
    pushal
    call interrupt_handler_47
    popal
    iretl

# Set the size of the _start symbol to the current location '.' minus its start.
# This is useful when debugging or when you implement call tracing.
.size _start, . - _start
