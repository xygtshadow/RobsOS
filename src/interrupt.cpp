#include <stddef.h>
#include <stdint.h>
#include "terminal.h"
#include "debug.h"
#include "pic.h"
#include "io.h"
#include "keyboard.h"

extern "C"{

    /** \brief Interrupt handler for interrupt 0: Divide Error
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_0(){
        terminal::writestring("Interrupt 0: Divide Error");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 1: Debug Exception
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_1(){
        terminal::writestring("Interrupt 1: Debug Exception");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 2: NMI Interrupt
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_2(){
        terminal::writestring("Interrupt 2: NMI Interrupt");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 3: Breakpoint
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_3(){
        terminal::writestring("Interrupt 3: Breakpoint");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 4: Overflow
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_4(){
        terminal::writestring("Interrupt 4: Overflow");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 5: BOUND Range Exceeded
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_5(){
        terminal::writestring("Interrupt 5: BOUND Range Exceeded");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 6: Invalid Opcode
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_6(){
        terminal::writestring("Interrupt 6: Invalid Opcode");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 7: Device Not Available
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_7(){
        terminal::writestring("Interrupt 7: Device Not Available");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 8: Double Fault
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_8(){
        terminal::writestring("Interrupt 8: Double Fault");
        terminal::putchar('\n');
        breakpoint();
    }

    /** \brief Interrupt handler for interrupt 9: Coprocessor Segment Overrun
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_9(){
        terminal::writestring("Interrupt 9: Coprocessor Segment Overrun");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 10: Invalid TSS
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_10(){
        terminal::writestring("Interrupt 10: Invalid TSS");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 11: Segment Not Present
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_11(){
        terminal::writestring("Interrupt 11: Segment Not Present");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 12: Stack-Segment Fault
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_12(){
        terminal::writestring("Interrupt 12: Stack-Segment Fault");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 13: General Protection
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_13(){
        terminal::writestring("Interrupt 13: General Protection");
        terminal::putchar('\n');
        breakpoint();
    }

    /** \brief Interrupt handler for interrupt 14: Page Fault
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_14(){
        terminal::writestring("Interrupt 14: Page Fault");
        terminal::putchar('\n');
        breakpoint();
    }

    /** \brief Interrupt handler for interrupt 16: x87 FPU Floating-Point Error
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_16(){
        terminal::writestring("Interrupt 16: x87 FPU Floating-Point Error");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 17: Alignment Check
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_17(){
        terminal::writestring("Interrupt 17: Alignment Check");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 18: Machine Check
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_18(){
        terminal::writestring("Interrupt 18: Machine Check");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 19: SIMD Floating-Point Exception
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_19(){
        terminal::writestring("Interrupt 19: SIMD Floating-Point Exception");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 20: Virtualization Exception
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_20(){
        terminal::writestring("Interrupt 20: Virtualization Exception");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 32: Programmable Interrupt Timer
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_32(){
        terminal::writestring("Interrupt 32: Programmable Interrupt Timer");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 33: Keyboard
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_33(){
        keyboard::interrupt();
        pic::sendEOI(1);
    }

    /** \brief Interrupt handler for interrupt 35: COM2
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_35(){
        terminal::writestring("Interrupt 35: COM2");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 36: COM1
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_36(){
        terminal::writestring("Interrupt 36: COM1");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 37: LPT2
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_37(){
        terminal::writestring("Interrupt 37: LPT2");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 38: Floppy Disk
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_38(){
        terminal::writestring("Interrupt 38: Floppy Disk");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 39: LPT1, possibly spurious
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_39(){
        if(pic::is_spurious(7)){
            terminal::writestring("Interrupt 39: Spurious");
        } else {
            terminal::writestring("Interrupt 39: LPT1");
        }
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 40: CMOS real-time clock
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_40(){
        terminal::writestring("Interrupt 40: CMOS real-time clock");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 41: Free/legacy SCSI/NIC
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_41(){
        terminal::writestring("Interrupt 41: Free for peripherals / legacy SCSI / NIC ");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 42: Free/SCSI/NIC
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_42(){
        terminal::writestring("Interrupt 42: Free for peripherals / SCSI / NIC ");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 43: Free/SCSI/NIC
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_43(){
        terminal::writestring("Interrupt 43: Free for peripherals / SCSI / NIC ");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 44: PS2 Mouse
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_44(){
        terminal::writestring("Interrupt 44: PS2 Mouse");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 45: FPU/Coprocessor/Inter-processor
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_45(){
        terminal::writestring("Interrupt 45: FPU/Coprocessor/Inter-processor");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 46: Primary ATA Hard Disk
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_46(){
        terminal::writestring("Interrupt 46: Primary ATA Hard Disk");
        terminal::putchar('\n');
    }

    /** \brief Interrupt handler for interrupt 47: Secondary ATA Hard Disk, possible spurious
     *         just prints text to terminal for now.
     *
     */
    void interrupt_handler_47(){
        if(pic::is_spurious(15)){
            terminal::writestring("Interrupt 47: Spurious");
        } else {
            terminal::writestring("Interrupt 47: Secondary ATA Hard Disk");
        }
        terminal::putchar('\n');
    }
}
