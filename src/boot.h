#ifndef BOOT_H
#define BOOT_H

#include "gdt.h"
#include "idt.h"
extern "C" {//from boot.s
    void setGdt(gdt::segment_descriptor*,size_t);
    void reloadSegments();
    void setIdt(idt::descriptor*,size_t);
    void interrupts_enable();
    void interrupts_disable();
    void halt();
    void enable_paging(uint32_t page_directory);
    void update_tlb();
}

#endif // BOOT_H
