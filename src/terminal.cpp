#include <stddef.h>
#include <stdint.h>
#include "string.h"
#include "terminal.h"
#include "io.h"
#include "debug.h"
#include "math.h"

namespace terminal{

    /** \brief Creates a color byte from the foreground and background parameters
     *
     * \param fg: Foreground (text) color
     * \param bg: Background color
     * \return uint8_t combined color
     *
     */
    uint8_t make_color(enum colors fg, enum colors bg){
        // 0xFB F=foreground color, B=background color
        return fg | bg << 4;
    }

    /** \brief Combines a character and a color into a word ready for printing to terminal
     *
     * \param c: character to print
     * \param color: combined foreground/background color of the character
     * \return uint16_t combined character and color
     *
     */
    uint16_t make_vgaentry(char c, uint8_t color){
        uint16_t c16 = c;
        uint16_t color16 = color;
        // 0xFFBB F=foreground color, B=background color
        return c16 | color16 << 8;
    }

    size_t row;
    size_t column;
    uint8_t color;
    uint16_t* buffer;
    uint16_t base_port;

    /** \brief Initializes the terminal.
     *
     * Zeroes row/column, sets color to grey on black,
     * sets up pointer/port, and clears the terminal
     */
    void initialize(){
        base_port = *((uint16_t*)0x0463);
        row=0;
        column=0;
        color=make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
        buffer=(uint16_t*) 0xB8000;
        for(size_t y =0;y<HEIGHT;y++){
            for(size_t x =0; x<WIDTH;x++){
                const size_t index = y * WIDTH + x;
                buffer[index] = make_vgaentry(' ', color);
            }
        }
        update_cursor(24,0);
    }

    /** \brief Sets the color of the terminal
     *
     * \param color: Foreground/background uint8_t color pair
     *
     */
    void setcolor(uint8_t color){
        color = color;
    }

    /** \brief Puts an character of a given color at a given position on the terminal
     *
     * \param c: Character to print
     * \param color: Foreground/background uint8_t color pair for the character
     * \param x: Zero based column to place character
     * \param y: Zero based row to place character
     *
     */
    void putentryat(char c, uint8_t color, size_t x, size_t y){
        const size_t index = y * WIDTH + x;
        buffer[index] = make_vgaentry(c, color);
    }

    /** \brief Scrolls the terminal up by one line, blanking the bottom line
     *
     */
    void scroll(){
        for(size_t col=0;col<WIDTH;col++){
            for(size_t r=0;r<HEIGHT-1;r++){
                size_t thisrow = r*WIDTH+col;
                size_t nextrow = (r+1)*WIDTH+col;
                buffer[thisrow]=buffer[nextrow];
            }
        }
        for(size_t col=0;col<WIDTH;col++){
            putentryat(' ',color, col, HEIGHT-1);
        }
    }

    /** \brief Puts a character a the current position
     *
     * \param c: character to place
     *
     */
    void putchar(char c){
        if(c!='\n')
            putentryat(c, color, column, row);
        if(++column == WIDTH || c=='\n'){
            column=0;
            if(row==HEIGHT-1)
                scroll();
            else row++;
        }
    }

    /** \brief Writes a string to the terminal
     *
     * \param data: Null terminated string
     *
     */
    void writestring(const char* data){
        size_t datalen = string::strlen(data);
        for(size_t i = 0; i<datalen;i++)
            putchar(data[i]);
    }

    /** \brief Writes a string to the terminal, adding a newline
     *
     * \param data: Null terminated string
     *
     */
    void writeline(const char* data){
        writestring(data);
        putchar('\n');
    }

    /** \brief Coverts a nibble (0-15) to it's hex character representation
     *
     * \param c: nibble to covert
     * \return hex character representation of the nibble
     *
     */
    char nibble_to_hexchar(uint8_t c){
        switch(c){
            case 0: return '0';break;
            case 1: return '1';break;
            case 2: return '2';break;
            case 3: return '3';break;
            case 4: return '4';break;
            case 5: return '5';break;
            case 6: return '6';break;
            case 7: return '7';break;
            case 8: return '8';break;
            case 9: return '9';break;
            case 10: return 'A';break;
            case 11: return 'B';break;
            case 12: return 'C';break;
            case 13: return 'D';break;
            case 14: return 'E';break;
            case 15: return 'F';break;
            default: return 'X';break;
        }
    }

    /** \brief Prints a hex representation of a byte to the terminal
     *
     * \param b: byte (uint8_t) to be printed
     *
     */
    void print_byte_hex(uint8_t b){
        uint8_t nibble1 = (b>>4) & 0xF;
        uint8_t nibble2 = b & 0xF;
        putchar(nibble_to_hexchar(nibble1));
        putchar(nibble_to_hexchar(nibble2));
    }

    /** \brief Prints a hex representation of a word to the terminal
     *
     * \param w: word (uint16_t) to be printed
     *
     */
    void print_word_hex(uint16_t w){
        uint8_t byte1 = (w>>8) & 0xFF;
        uint8_t byte2 = w & 0xFF;
        print_byte_hex(byte1);
        print_byte_hex(byte2);
    }

    /** \brief Prints a hex representation of a dword to the terminal
     *
     * \param d: dword (uint32_t) to be printed
     *
     */
    void print_dword_hex(uint32_t d){
        uint16_t word1 = (d>>16) & 0xFFFF;
        uint16_t word2 = d & 0xFFFF;
        writestring("0x");
        print_word_hex(word1);
        print_word_hex(word2);
    }

    /** \brief Prints a decimal representation of a dword to the terminal
     *
     * \param dw: dword (uint32_t) to be printed
     *
     */
    void print_dword_dec(uint32_t dw){
        char cstr[11] = {0};
        for(size_t i=0;i<10;i++){
            uint32_t d = math::pow(10,9-i);//decimal unit value
            cstr[i]=nibble_to_hexchar(dw/d);//calc decimal value
            dw=dw % d;//remove upper decimal
        }
        writestring(&cstr[0]);
    }

    /** \brief Updates the cursor (blinking underscore) position of the terminal
     *
     * \param row: Zero based row to place cursor
     * \param col: Zero based column to place cursor
     *
     */
     void update_cursor(int row, int col)
     {
        unsigned short position=(row*80) + col;

        // cursor LOW port to vga INDEX register
        outb(base_port, 0x0F);
        outb(base_port+1, (unsigned char)(position&0xFF));
        // cursor HIGH port to vga INDEX register
        outb(base_port, 0x0E);
        outb(base_port+1, (unsigned char )((position>>8)&0xFF));
     }

     void endl(){
         putchar('\n');
     }
}
