#ifndef IO_H
#define IO_H

#include <stddef.h>
#include <stdint.h>

extern "C" {
    void outb(uint16_t port, uint8_t byte);
    uint8_t inb(uint16_t port);
    void io_wait();
}

#endif
