#include <stddef.h>
#include <stdint.h>
#include "math.h"

namespace math{

    /** \brief Integer exponentiation, no safety
     *
     * \param base: base of the exponent
     * \param exp: the exponent value
     * \return product uint32_t
     *
     */
    uint32_t pow(uint32_t base, uint32_t exp){
        uint32_t tot =1;
        while(exp>0){
            tot*=base;
            exp--;
        }
        return tot;
    }
}
