#ifndef STRING_H
#define STRING_H

namespace string{

    size_t strlen(const char* str);
    size_t strcmp(const char* left, const char* right);
    char* strcpy(const char* str, size_t start, uint32_t len);
    int32_t indexof(const char* str, const char* pat);

}

#endif
