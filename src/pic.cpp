#include <stddef.h>
#include <stdint.h>
#include "io.h"

namespace pic {

    #define PIC1_CMD  0x0020
    #define PIC1_DATA     0x0021
    #define PIC2_CMD   0x00A0
    #define PIC2_DATA      0x00A1

    #define PIC_EOI  0x20  /* end of interrupt command code */

    /** \brief Sends an End of Interrupt code to the irq line
     *
     * \param irq: irq line to be EOId (0-15)
     *
     */

    void sendEOI(uint8_t irq){
        if(irq>=8)
            outb(PIC2_CMD, PIC_EOI);
        outb(PIC1_CMD, PIC_EOI);
    }

    #define ICW1_ICW4	0x01		/* ICW4 (not) needed */
    #define ICW1_SINGLE	0x02		/* Single (cascade) mode */
    #define ICW1_INTERVAL4	0x04		/* Call address interval 4 (8) */
    #define ICW1_LEVEL	0x08		/* Level triggered (edge) mode */
    #define ICW1_INIT	0x10		/* Initialization - required! */

    #define ICW4_8086	0x01		/* 8086/88 (MCS-80/85) mode */
    #define ICW4_AUTO	0x02		/* Auto (normal) EOI */
    #define ICW4_BUF_SLAVE	0x08		/* Buffered mode/slave */
    #define ICW4_BUF_MASTER	0x0C		/* Buffered mode/master */
    #define ICW4_SFNM	0x10		/* Special fully nested (not) */

    /** \brief Initializes the Programmable Interrupt Controller, two sets of 8 irq lines
     *
     * \param offset1: Interrupt offset for the first 8 irq lines (typically 32)
     * \param offset2: Interrupt offset for the second 8 irq lines (typically 40)
     *
     */

    void initialize(uint16_t offset1, uint16_t offset2) {
        uint8_t a1, a2;
        a1 = inb(PIC1_DATA); //save masks
        a2 = inb(PIC2_DATA);

        //start initialization sequence in cascade mode
        outb(PIC1_CMD, ICW1_INIT+ICW1_ICW4);
        io_wait();
        outb(PIC2_CMD, ICW1_INIT+ICW1_ICW4);
        io_wait();
        // ICW2: PIC vector offset
        outb(PIC1_DATA, offset1);
        io_wait();
        outb(PIC2_DATA, offset2);
        io_wait();
        // ICW3: tell master PIC that there is a slave PIC at IRQ2 0000 0100
        outb(PIC1_DATA, 4);
        io_wait();
        // ICW3: tell slave PIC its cascade identity 0000 0010
        outb(PIC2_DATA, 2);
        io_wait();
        // ICW4: additional information about environment
        outb(PIC1_DATA, ICW4_8086);
        io_wait();
        outb(PIC2_DATA, ICW4_8086);
        io_wait();

        //restore saved masks
        outb(PIC1_DATA, a1);
        outb(PIC2_DATA, a2);
    }

    /** \brief Masks all IRQ lines on the PIC, disabling all interrupts
     *
     */

    void disable(){
        outb(PIC1_DATA, 0xFF);
        outb(PIC2_DATA, 0xFF);
    }

    /** \brief Disables a given IRQ line
     *
     * \param IRQline: IRQ line to disable
     *
     */

    void IRQ_set_mask(uint8_t IRQline){
        uint16_t port;
        uint8_t value;
        if(IRQline < 8)
            port=PIC1_DATA;
        else{
            port=PIC2_DATA;
            IRQline -= 8;
        }
        value = inb(port) | (1<<IRQline);
        outb(port, value);
    }

    /** \brief Enables a given IRQ lines
     *
     * \param IRQline: IRQ line to enable
     *
     */

    void IRQ_clear_mask(uint8_t IRQline){
        uint16_t port;
        uint8_t value;
        if(IRQline < 8)
            port=PIC1_DATA;
        else{
            port=PIC2_DATA;
            IRQline -= 8;
        }
        value = inb(port) & ~(1 << IRQline);
        outb(port, value);
    }

    #define PIC_READ_IRR  0x0A /* OCW3 irq ready next CMD read */
    #define PIC_READ_ISR  0x0B /* OCW3 irq service next CMD read */

    /* Helper func */
    static uint16_t __pic_get_irq_reg(int ocw3)
    {
        /* OCW3 to PIC CMD to get the register values.  PIC2 is chained, and
        * represents IRQs 8-15.  PIC1 is IRQs 0-7, with 2 being the chain */
        outb(PIC1_CMD, ocw3);
        outb(PIC2_CMD, ocw3);
        return (inb(PIC2_CMD) << 8) | inb(PIC1_CMD);
    }
    /* Returns the combined value of the cascaded PICs irq request register */
    uint16_t get_irr(void)
    {
        return __pic_get_irq_reg(PIC_READ_IRR);
    }

    /* Returns the combined value of the cascaded PICs in-service register */
    uint16_t get_isr(void)
    {
        return __pic_get_irq_reg(PIC_READ_ISR);
    }

    bool is_spurious(uint8_t IRQline){
        return get_isr() & (1<<IRQline);
    }
}
