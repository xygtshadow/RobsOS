# $@ = file name of target rule (output)
# $+ = names of all prerequisites in order (multiple input)
# $+ = names of all prerequisites in order unique (multiple input)
# $< = name of first prerequisite (single input)
# $(X) = expand variable X

OD=obj
SRCDIR=src
BINDIR=bin
CFGDIR=cfg
ISODIR=iso
AS=i686-elf-as
ASFLAGS=
CXX=i686-elf-g++
CXXFLAGS=-ffreestanding -O0 -Wall -Wextra -fno-exceptions -fno-rtti -Wno-missing-field-initializers
LD=i686-elf-g++
LDFLAGS=-T $(SRCDIR)/linker.ld -ffreestanding -O0 -nostdlib -lgcc
OSNAME=RobsOS

all: $(OSNAME).iso

$(OD)/%.o: $(SRCDIR)/%.s
	$(AS) $(ASFLAGS) -I $(SRCDIR) $< -o $@

$(OD)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -I $(SRCDIR) -c $< -o $@

#boot.o must be at start, end.o must be at end
$(BINDIR)/$(OSNAME).bin: $(OD)/boot.o $(OD)/kernel.o $(OD)/terminal.o \
	$(OD)/string.o $(OD)/gdt.o $(OD)/debug.o $(OD)/idt.o \
	$(OD)/interrupt.o $(OD)/pic.o $(OD)/keyboard.o $(OD)/shell.o \
	$(OD)/multiboot.o $(OD)/math.o $(OD)/memory.o $(OD)/end.o
	$(LD) $(LDFLAGS) $^ -o $@

$(OSNAME).iso: $(BINDIR)/$(OSNAME).bin $(CFGDIR)/grub.cfg
	cp $(BINDIR)/$(OSNAME).bin $(ISODIR)/boot/$(OSNAME).bin
	cp $(CFGDIR)/grub.cfg $(ISODIR)/boot/grub/grub.cfg
	grub-mkrescue -o $(OSNAME).iso iso

clean:
	$(RM) $(OD)/*.o $(BINDIR)/*.bin $(ISODIR)/boot/$(OSNAME).bin $(ISODIR)/boot/grub/grub.cfg $(OSNAME).iso
